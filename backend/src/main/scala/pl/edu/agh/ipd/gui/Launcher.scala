package pl.edu.agh.ipd.gui

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging
import pl.edu.agh.ipd.actors.MasterArenaActor
import pl.edu.agh.ipd.config.IpdConfig
import pl.edu.agh.ipd.gui.config.GUIBackendConfig
import pl.edu.agh.ipd.gui.framework.FrameworkConnector
import pl.edu.agh.ipd.gui.jetty.ApplicationServer

object Launcher extends StrictLogging {
  def main(args: Array[String]): Unit = {
    import scala.concurrent.ExecutionContext.Implicits.global

    val configName: String = args(0)
    val startGUIServer: Boolean = args(1).toBoolean

    val akkaConfig = ConfigFactory.load(configName)
    val actorSystem: ActorSystem = ActorSystem.apply(name = pl.edu.agh.ipd.actors.actorSystemName, akkaConfig)
    val framework: FrameworkConnector = new FrameworkConnector(actorSystem)

    if (startGUIServer) {
      val server = new ApplicationServer(GUIBackendConfig.Server.port, GUIBackendConfig.Server.staticsPath, framework)
      server.start()
    }
  }
}
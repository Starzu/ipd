package pl.edu.agh.ipd.gui.config

import com.typesafe.config.{Config, ConfigFactory}

import scala.collection.JavaConversions._

object GUIBackendConfig {
  private val config: Config = ConfigFactory.load()

  object Server {
    private val subconfig = config.getConfig("server")

    val port: Int = subconfig.getInt("port")
    val staticsPath: String = subconfig.getString("statics")
    val baseUrl: String = subconfig.getString("baseUrl")
  }
}

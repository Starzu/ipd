package pl.edu.agh.ipd.gui.config

import pl.edu.agh.ipd.config.IpdConfig.{Payoffs => IpdPayoffs}
import pl.edu.agh.ipd.gui.config.SimulationConfig.{ArenaMap, Splitter, Payoffs => GUIPayoffs}

object Interop {
  implicit def guiPayoffs2ipdPayoffs(payoffs: GUIPayoffs): IpdPayoffs =
    IpdPayoffs(payoffs.temptation, payoffs.reward, payoffs.punishment, payoffs.sucker)

  implicit class MapOps(val map: ArenaMap) extends AnyVal {
    def options: Map[String, Any] = map match {
      case ArenaMap.OneDimMap => Map.empty
      case ArenaMap.TwoDimMap(width: Int) => Map("width" -> width)
      case ArenaMap.Random => Map()
      case ArenaMap.GraphMap(filename: String) => Map("filename" -> filename)
    }
  }

  implicit class SplitterOps(val splitter: Splitter) extends AnyVal {
    def options: Map[String, Any] = splitter match {
      case Splitter.DummySplitter => Map.empty
      case Splitter.GraphSplitter(filename: String) => Map("filename" -> filename)
    }
  }
}

package pl.edu.agh.ipd.gui.framework

import akka.actor.{ActorRef, ActorSelection, ActorSystem}
import akka.pattern.ask
import akka.util.Timeout
import pl.edu.agh.ipd.actors.MasterStatsActor.MasterStats
import pl.edu.agh.ipd.actors.{MasterArenaActor, MasterStatsActor}
import pl.edu.agh.ipd.config.IpdConfig
import pl.edu.agh.ipd.gui.config.SimulationConfig
import pl.edu.agh.ipd.model.Strategies

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

class FrameworkConnector(actorSystem: ActorSystem)(implicit ec: ExecutionContext) {
  implicit val timeout: Timeout = 5 seconds

  private val ArenaName: String = "arena"
  private var arena: ActorRef = _

  val masterStats: ActorSelection = actorSystem.actorSelection(s"/user/$ArenaName/${MasterArenaActor.MasterStatsName}")

  def start(config: SimulationConfig): Unit = {
    require(arena == null)

    import pl.edu.agh.ipd.gui.config.Interop._
    val ipdConfig = IpdConfig(
      config.populationSize,
      config.initialStrategy.name,
      config.separateStrategies,
      config.initialEnergy,
      config.payoffs,
      config.splitter.className,
      config.splitter.options,
      config.useStubs,
      config.map.className,
      config.map.options,
      config.map.className, //todo separate crossover map
      config.map.options, //todo separate crossover map
      config.targetFightsToMutationsRatio,
      config.crossoverMgrsCount,
      config.fightMgrsCount,
      config.mutationMgrsCount,
      config.replaceEnergyOnMutation,
      config.forceEpochDeaths,
      config.fightTimeoutMs,
      config.continuationProbability,
      config.stubSyncIntervalMs,
      config.stubSyncBatchThreshold,
      config.localStatsIntervalMs,
      config.masterStatsIntervalMs,
      config.topNStrategies,
      config.availableHosts
    )
    arena = actorSystem.actorOf(MasterArenaActor.props(ipdConfig), ArenaName)
    arena ! MasterArenaActor.Init
  }

  def stop(): Unit = {
    require(arena != null)
    arena ! MasterArenaActor.Kill
    arena = null
  }

  def isRunning: Boolean =
    arena != null

  def stats(): Future[String] =
    (masterStats ? MasterStatsActor.CurrentStats).map(_.asInstanceOf[MasterStats].pretty)

  def resetStats(): Future[Unit] =
    Future(masterStats ! MasterStatsActor.ClearStats)

}

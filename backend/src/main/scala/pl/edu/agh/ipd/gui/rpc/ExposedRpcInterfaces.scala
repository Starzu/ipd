package pl.edu.agh.ipd.gui.rpc

import io.udash.rpc._
import pl.edu.agh.ipd.gui.config.SimulationConfig
import pl.edu.agh.ipd.gui.framework.FrameworkConnector

import scala.concurrent.Future
import scala.language.postfixOps

class ExposedRpcInterfaces(framework: FrameworkConnector)(implicit clientId: ClientId) extends MainServerRPC {
  override def start(config: SimulationConfig): Future[Unit] =
    Future(framework.start(config))

  override def stop(): Future[Unit] =
    Future(framework.stop())

  override def stats(): Future[String] =
    framework.stats()

  override def resetStats(): Future[Unit] =
    framework.resetStats()

  override def isRunning(): Future[Boolean] =
    Future.successful(framework.isRunning)
}

       
package pl.edu.agh.ipd.gui

import scala.concurrent.ExecutionContext

package object rpc {
  implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global
}

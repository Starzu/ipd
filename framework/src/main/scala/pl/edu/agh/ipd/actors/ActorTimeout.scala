package pl.edu.agh.ipd.actors

import akka.actor.{Actor, Cancellable}

import scala.concurrent.duration.FiniteDuration

trait ActorTimeout[T] { this: Actor =>
  import context.dispatcher

  def timeoutMessage: T
  def timeout: FiniteDuration

  private var timeoutCancellable: Cancellable = _

  def restartTimeout(): Unit = {
    cancelTimeout()
    timeoutCancellable = context.system.scheduler.scheduleOnce(timeout, self, timeoutMessage)
  }

  def continue(): Unit =
    self ! timeoutMessage

  def cancelTimeout(): Unit =
    if (timeoutCancellable != null)
      timeoutCancellable.cancel()
}

package pl.edu.agh.ipd.actors

import akka.actor.{Actor, ActorLogging, ActorSelection, Props}
import akka.event.LoggingReceive
import akka.pattern.ask
import akka.routing.{ActorRefRoutee, RoundRobinRoutingLogic, Router}
import akka.util.Timeout
import com.github.ghik.silencer.silent
import pl.edu.agh.ipd.actors.CrossoverMgrActor.NewStrategyRequest
import pl.edu.agh.ipd.model.{Mutations, Strategies, Strategy, StrategyView}

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration
import scala.util.Random

object CrossoverMgrActor {
  def props(routeesNum: Int, map: ActorSelection, timeout: FiniteDuration): Props =
    if (routeesNum < 2) Props(classOf[CrossoverMgrActor], map, timeout)
    else Props(classOf[CrossoverRouter], routeesNum, map, timeout)

  sealed trait Message
  case class NewStrategyRequest(prisoner: ActorSelection) extends Message
}

class CrossoverRouter(val routeesNum: Int, val map: ActorSelection, timeout: FiniteDuration) extends Actor {
  private val router = {
    val routees = Vector.fill(routeesNum) {
      ActorRefRoutee(
        context.actorOf(CrossoverMgrActor.props(1, map, timeout))
      )
    }
    Router(RoundRobinRoutingLogic(), routees)
  }

  override def receive: Receive = {
    case m: NewStrategyRequest =>
      router.route(m, sender())
  }
}

class CrossoverMgrActor(val map: ActorSelection, timeout: FiniteDuration) extends Actor with ActorLogging {
  import pl.edu.agh.ipd.actors.map.ArenaMapActor._

  implicit val akkaTimeout = Timeout(timeout)
  implicit val ec = context.system.dispatcher

  override def receive: Receive = {
    case NewStrategyRequest(prisoner) =>
      val arena = sender()
      val find = map ? FindNeighbours(prisoner)
      find onSuccess {
        case neighbours: Neighbourhood @silent if neighbours.length >= 2 =>
          val i = Random.nextInt(neighbours.length)
          val j = {
            var tmp = i
            while (i == tmp) tmp = Random.nextInt(neighbours.length)
            tmp
          }

          val parentA: ActorSelection = neighbours(i)
          val parentB: ActorSelection = neighbours(j)
          val strategies = Future.sequence(Seq(
            parentA ? PrisonerActor.StrategyRequest,
            parentB ? PrisonerActor.StrategyRequest
          ))
          strategies onSuccess {
            case Seq(strategyA: StrategyView, strategyB: StrategyView) =>
              arena ! Mutations.mutate(Strategy.crossover(strategyA, strategyA))
          }
          strategies.onFailure({case _ => log.warning("No response from neighbours.")})
        case neighbours: Neighbourhood @silent if neighbours.length == 1 =>
          neighbours.head ? PrisonerActor.StrategyRequest onSuccess {
            case strategy: StrategyView =>
              //TODO trigger mutation?
              arena ! Mutations.mutate(strategy.copy())
          }
        case _ =>
          log.warning("No neighbours found.")
      }
      find.onFailure({case _ => log.warning("No response from map.")})
  }
}

package pl.edu.agh.ipd.actors

import java.util.concurrent.atomic.AtomicLong

import akka.actor.{Actor, ActorLogging, ActorSelection, Props}
import com.github.ghik.silencer.silent
import pl.edu.agh.ipd.actors.FightMgrActor.FightId
import pl.edu.agh.ipd.actors.LocalStatsActor.FightStarted
import pl.edu.agh.ipd.actors.map.ArenaMapActor
import pl.edu.agh.ipd.config.IpdConfig.Payoffs

import scala.concurrent.duration.FiniteDuration
import scala.language.postfixOps
import scala.util.{Random, Try}

object FightMgrActor {
  var i = new AtomicLong(0)
  def props(prisoners: Seq[ActorSelection], map: ActorSelection, stats: ActorSelection, payoffs: Payoffs,
            fightTimeout: FiniteDuration, continuationProbability: Double, forceEpochDeaths: Boolean,
            createFightId: => FightId = FightId(i.incrementAndGet() /*UUID.randomUUID()*/)): Props =
    Props(classOf[FightMgrActor], prisoners, map, stats, payoffs, fightTimeout, continuationProbability, forceEpochDeaths, createFightId _)

  sealed trait Message
  sealed trait StartFight extends Message
  case object NextFight extends StartFight
  case object TimeoutFight extends StartFight

  case class FightId(id: Long) extends AnyVal
  case class MessageWithFightId(fightId: FightId, message: Any)
}

class FightMgrActor(val prisoners: Seq[ActorSelection],
                    map: ActorSelection,
                    stats: ActorSelection,
                    payoffs: Payoffs,
                    override val timeout: FiniteDuration,
                    continuationProbability: Double,
                    forceEpochDeaths: Boolean,
                    createFightId: => FightId)
  extends Actor with ActorLogging with ActorTimeout[FightMgrActor.TimeoutFight.type] {

  import ArenaMapActor._
  import FightMgrActor._

  override val timeoutMessage = TimeoutFight

  private var stopped: Boolean = false
  private var fightId: FightId = _
  private var prisoner: ActorSelection = _
  private var opponent: ActorSelection = _
  private var prisonerMove: PrisonerActor.Action = _
  private var opponentMove: PrisonerActor.Action = _

  private var collectStats: Boolean = false

  private var standardFights: Long = 0
  private var remoteFights: Long = 0

  override def receive: Receive = {
    case TimeoutFight =>
      if (collectStats) stats ! LocalStatsActor.FightTimeout(isRemoteFight())
      log.warning(s"Timeout: ${Try(prisoner.pathString).getOrElse("-")} ${Try(opponent.pathString).getOrElse("-")} - $prisonerMove $opponentMove")
      prepareFight(true)
    case NextFight =>
      prepareFight(false)
    case LocalArenaActor.Stop =>
      cancelTimeout()
      stopped = true
    case MessageWithFightId(id, message) if id == fightId => message match {
      case neighbours: Neighbourhood @silent =>
        if (neighbours.nonEmpty) {
          opponent = neighbours(Random.nextInt(neighbours.length))
          askForNextMoves()
        }
      case move: PrisonerActor.Action =>
        if (sender().path.name == prisoner.name) prisonerMove = move
        else if (sender().path.name == opponent.name) opponentMove = move
        if (prisonerMove != null && opponentMove != null) {
          resolveFight(prisonerMove, opponentMove)
          self ! NextFight
        }
    }

    case MasterStatsActor.StartStats =>
      collectStats = true
  }

  private def askForNextMoves() = {
    prisoner ! MessageWithFightId(fightId, PrisonerActor.NextMoveRequest)
    opponent ! MessageWithFightId(fightId, PrisonerActor.NextMoveRequest)
  }

  protected def isRemoteFight(): Boolean =
    prisoner != null && opponent != null &&
      prisoner.anchorPath.root.address != opponent.anchorPath.root.address

  protected def prepareFight(timeout: Boolean): Unit = if (!stopped) {
    restartTimeout()
    if (!timeout && prisoner != null && opponent != null && Random.nextDouble() < continuationProbability) {
      prisonerMove = null
      opponentMove = null
      askForNextMoves()
    } else {
      if(forceEpochDeaths) {
        if (prisoner != null && opponent != null) {
          prisoner ! PrisonerActor.ForceDeath
          opponent ! PrisonerActor.ForceDeath
        }
      }

      fightId = createFightId
      prisonerMove = null
      opponentMove = null
      prisoner = null
      opponent = null
      prisoner = prisoners(Random.nextInt(prisoners.length))
      map ! MessageWithFightId(fightId, ArenaMapActor.FindNeighbours(prisoner))
    }
  }

  protected def resolveFight(moveA: PrisonerActor.Action, moveB: PrisonerActor.Action): Unit = {
    val (payoffA, payoffB) = PrisonerActor.Action.resolveFight(payoffs)(moveA, moveB)

    if (collectStats) {
      if (isRemoteFight()) remoteFights += 1
      else standardFights += 1

      if (remoteFights + standardFights > 50) {
        stats ! FightStarted(standardFights, remoteFights)
        standardFights = 0
        remoteFights = 0
      }
    }

    prisoner ! MessageWithFightId(fightId, PrisonerActor.FightResult(moveB, payoffA))
    opponent ! MessageWithFightId(fightId, PrisonerActor.FightResult(moveA, payoffB))
  }
}

package pl.edu.agh.ipd.actors

import java.util.concurrent.TimeUnit

import akka.pattern.ask
import akka.actor.{Actor, ActorLogging, ActorPath, ActorRef, ActorSelection, AddressFromURIString, Deploy, Props, Terminated}
import akka.remote.RemoteScope
import akka.util.Timeout
import pl.edu.agh.ipd.actors.map.{ArenaMapActor, EnergyProportionalSamplingMapActor, SimilarCooperativityMapActor}
import pl.edu.agh.ipd.config.IpdConfig
import pl.edu.agh.ipd.model.{Strategy, StrategyView}

import scala.concurrent.duration.{DurationDouble, FiniteDuration}
import scala.language.postfixOps
import scala.util.Success

object LocalArenaActor {
  def props(hostId: Int, config: IpdConfig, master: ActorSelection, masterStats: ActorSelection,
            prisonerHosts: Map[Int, Seq[Int]], stubsNums: Seq[Int]): Props =
    Props(classOf[LocalArenaActor], hostId, config, master, masterStats, prisonerHosts, stubsNums)

  trait Message extends Serializable
  case object Init extends Message
  case object Stop extends Message
  case object KillMe extends Message

  def localStatsName(hostId: Int): String =
    s"localStats-$hostId"
}

class LocalArenaActor(hostId: Int, config: IpdConfig, master: ActorSelection, masterStats: ActorSelection, prisonerHosts: Map[Int, Seq[Int]], stubsNums: Seq[Int]) extends Actor with ActorLogging {
  import LocalArenaActor._

  implicit val replaceTimeout = Timeout(config.fightTimeoutMs millis)
  implicit val ec = context.system.dispatcher

  private var collectStats: Boolean = false

  private def deployConfig(host: String): Deploy =
    Deploy(scope = RemoteScope(AddressFromURIString(s"akka.tcp://$actorSystemName@$host")))

  log.info(s"Starting local arena `${self.path.name}` with ${prisonerHosts(hostId).size}")
  require(prisonerHosts(hostId).toSet.intersect(stubsNums.toSet).isEmpty)

  val localStats: ActorSelection =
    context.actorSelection(context.actorOf(
      LocalStatsActor.props(masterStats, config.masterStatsIntervalMs millis, config.topNStrategies).withDeploy(deployConfig(config.availableHosts(hostId))),
      LocalArenaActor.localStatsName(hostId)
    ).path)

  val prisoners: Seq[ActorSelection] = prisonerHosts(hostId).map(id => {
    val actor: ActorRef = context.actorOf(
      PrisonerActor.props(config.initialEnergy, config.initialStrategy(), localStats,
        config.localStatsIntervalMs millis, 2 * config.fightTimeoutMs, config.separateStrategies
      ).withDeploy(deployConfig(config.availableHosts(hostId))),
      PrisonerActor.prisonerName(id)
    )
    context.watch(actor)
    context.actorSelection(actor.path)
  })

  val allPrisoners: Seq[ActorSelection] = prisonerHosts.toSeq.flatMap({
    case (0, prs) if 0 != hostId =>
      prs.map(prisonerId => {
        val path = ActorPath.fromString(
          s"akka.tcp://$actorSystemName@${config.availableHosts.head}/user/arena/localArena-0/${PrisonerActor.prisonerName(prisonerId)}"
        )
        (prisonerId, context.actorSelection(path))
      })
    case (pHostId, prs) if pHostId != hostId =>
      prs.map(prisonerId => {
        val path = ActorPath.fromString(
          s"akka.tcp://$actorSystemName@${config.availableHosts(pHostId)}/remote/akka.tcp/$actorSystemName@${config.availableHosts.head}/user/arena/localArena-$pHostId/${PrisonerActor.prisonerName(prisonerId)}"
        )
        (prisonerId, context.actorSelection(path))
      })
    case _ => prisoners.map(as => (PrisonerActor.prisonerId(as.name), as)) //TODO access prisoner id better
  }).map({
    case (id, as) if stubsNums.contains(id) =>
      val stub = context.actorSelection(context.actorOf(
        PrisonerStubActor.props(config.initialStrategy(), as, config.stubSyncBatchThreshold,
          config.stubSyncIntervalMs millis, 2 * config.fightTimeoutMs, config.separateStrategies
        ).withDeploy(deployConfig(config.availableHosts(hostId))),
        PrisonerActor.prisonerName(id, isStub = true)
      ).path)
      (id, stub)
    case (id, as) => (id, as)
  }).sortBy(_._1).map(_._2)

  private val cooperativityProvider = new SimilarCooperativityMapActor.ActorCooperativityProvider(masterStats, config.masterStatsIntervalMs millis, config.populationSize)
  private val localStrategiesProvider = new EnergyProportionalSamplingMapActor.ActorPrisonersEnergyProvider(masterStats, config.masterStatsIntervalMs millis)

  val map: ActorSelection =
    context.actorSelection(context.actorOf(
      ArenaMapActor.props(
        config.map, allPrisoners,
        config.mapOptions
          .updated("cooperativityProvider", cooperativityProvider)
          .updated("localStrategiesProvider", localStrategiesProvider)
      ).withDeploy(deployConfig(config.availableHosts(hostId))),
      s"map-$hostId"
    ).path)

  val crossoverMap: ActorSelection =
    context.actorSelection(context.actorOf(
      ArenaMapActor.props(
        config.crossoverMap, allPrisoners,
        config.crossoverMapOptions
          .updated("cooperativityProvider", cooperativityProvider)
          .updated("localStrategiesProvider", localStrategiesProvider)
      ).withDeploy(deployConfig(config.availableHosts(hostId))),
      s"crossoverMap-$hostId"
    ).path)

  val fightMgrs: Seq[ActorSelection] =
    IndexedSeq.tabulate(config.fightMgrsCount)(id =>
      context.actorSelection(context.actorOf(
        FightMgrActor.props(prisoners, map, localStats, config.payoffs,
          FiniteDuration(config.fightTimeoutMs, TimeUnit.MILLISECONDS), config.continuationProbability, config.forceEpochDeaths)
          .withDeploy(deployConfig(config.availableHosts(hostId))),
        s"fightMgr-${id + hostId * config.fightMgrsCount}"
      ).path)
    )

  val mutationMgrs: Seq[ActorSelection] =
    IndexedSeq.tabulate(config.mutationMgrsCount)(id =>
      context.actorSelection(context.actorOf(
        MutationMgrActor.props(
          prisoners, localStats, config.targetFightsToMutationsRatio, config.mutationMgrsCount,
          if (config.replaceEnergyOnMutation) Some(config.initialEnergy) else None, config.fightTimeoutMs millis
        ).withDeploy(deployConfig(config.availableHosts(hostId))),
        s"mutationMgr-${id + hostId * config.fightMgrsCount}"
      ).path)
    )

  val crossoverMgrActor: ActorSelection =
    context.actorSelection(context.actorOf(
      CrossoverMgrActor.props(config.crossoverMgrsCount, crossoverMap, config.fightTimeoutMs millis)
        .withDeploy(deployConfig(config.availableHosts(hostId))),
      s"crossoverMgr-$hostId"
    ).path)

  override def receive: Receive = {
    case Init =>
      fightMgrs.foreach(_ ! FightMgrActor.NextFight)
      mutationMgrs.foreach(_ ! MutationMgrActor.StartMutation)

    case Stop =>
      fightMgrs.foreach(_ ! LocalArenaActor.Stop)
      mutationMgrs.foreach(_ ! LocalArenaActor.Stop)

    case MasterStatsActor.StartStats =>
      collectStats = true
      fightMgrs.foreach(_ ! MasterStatsActor.StartStats)
      mutationMgrs.foreach(_ ! MasterStatsActor.StartStats)
      prisoners.foreach(_ ! MasterStatsActor.StartStats)

    case KillMe =>
      val prisoner = sender()
      if (collectStats) localStats ! LocalStatsActor.PrisonerDied(prisoner.path.name)
      crossoverMgrActor ? CrossoverMgrActor.NewStrategyRequest(context.actorSelection(prisoner.path)) onComplete {
        case Success(strategy: StrategyView) =>
          prisoner ! PrisonerActor.Restart(config.initialEnergy, strategy.copy())
        case _ =>
          self.tell(KillMe, prisoner)
      }

    case Terminated(ref) =>
      if (collectStats) localStats ! LocalStatsActor.PrisonerDied(ref.path.name)
      replacePrisoner(ref)

    case MasterArenaActor.Kill =>
      context.system.terminate()
  }

  def replacePrisoner(ref: ActorRef): Unit = {
    val strategyF = crossoverMgrActor ? CrossoverMgrActor.NewStrategyRequest(context.actorSelection(ref.path))

    strategyF onSuccess {
      case strategy: StrategyView =>
        context.watch(
          context.actorOf(
            PrisonerActor.props(config.initialEnergy, strategy.copy(), localStats,
              config.localStatsIntervalMs millis, 2 * config.fightTimeoutMs, config.separateStrategies
            ).withDeploy(deployConfig(config.availableHosts(hostId))),
            ref.path.name
          )
        )
    }

    strategyF onFailure {
      case _ =>
        replacePrisoner(ref)
    }
  }
}

package pl.edu.agh.ipd.actors

import java.util.Date

import akka.actor.{Actor, ActorLogging, ActorSelection, Props}
import akka.event.LoggingReceive
import com.typesafe.scalalogging.LazyLogging
import pl.edu.agh.ipd.actors.PrisonerActor.Cooperate
import pl.edu.agh.ipd.model.Strategy.Cooperativity
import pl.edu.agh.ipd.model.StrategyView
import pl.edu.agh.ipd.model.map.EnergyProportionalSamplingMap.StrategyStats

import scala.collection.mutable
import scala.concurrent.duration.{DurationInt, FiniteDuration}
import scala.language.postfixOps
import scala.util.Try

object LocalStatsActor {
  trait Message

  /**
    * @param fights Fights since last sent state
    * @param mutations Mutations since last sent state
    */
  case class PrisonerState(energy: PrisonerActor.Energy, fights: Long, lastCooperations: Int, allMoves: Int, mutations: Long,
                           strategy: StrategyView, energyChangeSinceMutation: PrisonerActor.Energy, fightsSinceMutation: Long) extends Message
  case class PrisonerDied(name: String) extends Message
  case class FightTimeout(remote: Boolean) extends Message
  case class FightStarted(standard: Long, remote: Long) extends Message
  case class MutationStarted(count: Long) extends Message
  case class Clear(epoch: Int) extends Message
  case object FightsToMutationsRequest extends Message
  case class FightsToMutations(fights: Long, mutations: Long) extends Message

  case object SendToMaster extends Message

  def props(master: ActorSelection, masterStatsInterval: FiniteDuration, topNStrategies: Int): Props =
    Props(classOf[LocalStatsActor], master, masterStatsInterval, topNStrategies)
}

class LocalStatsActor(master: ActorSelection, masterStatsInterval: FiniteDuration, topNStrategies: Int) extends Actor with ActorLogging with LazyLogging {
  import LocalStatsActor._

  private var epoch: Int = 0
  private val prisonersStats = mutable.Map.empty[String, PrisonerState]
  private val fights = mutable.Map.empty[String, Long].withDefaultValue(0)
  private val mutations = mutable.Map.empty[String, Long].withDefaultValue(0)
  private var deaths = 0
  private var fightTimeouts = 0
  private var remoteFightTimeouts = 0

  private var standardFightsStarted = 0L
  private var remoteFightsStarted = 0L
  private var mutationsStarted = 0L

  import context.dispatcher
  context.system.scheduler.schedule(initialDelay(masterStatsInterval), masterStatsInterval, self, SendToMaster)

  override def receive: Receive = {
    case state: PrisonerState =>
      val name: String = sender().path.name
      prisonersStats(name) = state
      fights(name) += state.fights
      mutations(name) += state.mutations
    case PrisonerDied(_) =>
      deaths += 1
    case FightTimeout(remote) =>
      fightTimeouts += 1
      if (remote) remoteFightTimeouts += 1
    case FightStarted(standard, remote) =>
      standardFightsStarted += standard
      remoteFightsStarted += remote
    case MutationStarted(count) =>
      mutationsStarted += count
    case SendToMaster =>
      sendToMaster()
    case FightsToMutationsRequest =>
      val f = fights.values.sum
      val m = mutations.values.sum
      sender() ! FightsToMutations(f, m)
    case Clear(ep) =>
      epoch = ep
      prisonersStats.clear()
      fights.clear()
      mutations.clear()
      deaths = 0
      fightTimeouts = 0
      remoteFightTimeouts = 0
      remoteFightsStarted = 0
  }

  private def sendToMaster(): Unit = {
    val energySum: Double = prisonersStats.values.map(_.energy).sum
    val energyMax: Double = prisonersStats.values.map(_.energy).maxOrElse(0)
    val energyMin: Double = prisonersStats.values.map(_.energy).minOrElse(0)
    val fightsSum: Long = fights.values.sum
    val fightsMax: Long = fights.values.maxOrElse(0)
    val fightsMin: Long = fights.values.minOrElse(0)
    val lastCooperationsCount: Long = prisonersStats.values.map(_.lastCooperations).sum
    val lastMovesCount: Long = prisonersStats.values.map(_.allMoves).sum
    val mutationsCount: Long = mutations.values.sum
    val mutationsMax: Long = mutations.values.maxOrElse(0)
    val mutationsMin: Long = mutations.values.minOrElse(0)
    val cooperativitySum: Cooperativity = prisonersStats.values.map(_.strategy.cooperativity).sum
    val bestStrategies = prisonersStats.toVector
      .sortBy(_._2.energy)
      .reverse
      .take(topNStrategies)
      .map { case (name, v) =>
        MasterStatsActor.StrategyInfo(name, v.energy, v.energyChangeSinceMutation, v.fightsSinceMutation, v.strategy)
      }
    val prisonersCooperativity = prisonersStats
      .map({ case (name, state) => (PrisonerActor.prisonerId(name), state.strategy.cooperativity)})
      .toMap

    master ! MasterStatsActor.LocalStats(
      epoch, prisonersStats.size, fightTimeouts, remoteFightTimeouts,
      energySum, energyMax, energyMin,
      standardFightsStarted + remoteFightsStarted, fightsSum, fightsMax, fightsMin,
      mutationsStarted, mutationsCount, mutationsMax, mutationsMin,
      remoteFightsStarted, lastCooperationsCount, lastMovesCount, deaths,
      cooperativitySum, bestStrategies,
      prisonersCooperativity,
      prisonersStats.map { case (name, value) => (PrisonerActor.prisonerId(name), value.energy) }.toMap
    )
  }
}

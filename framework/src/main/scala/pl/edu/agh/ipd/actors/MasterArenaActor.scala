package pl.edu.agh.ipd.actors

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSelection, AddressFromURIString, Deploy, PoisonPill, Props, Terminated}
import akka.event.LoggingReceive
import akka.remote.RemoteScope
import pl.edu.agh.ipd.actors.splitter.{ArenaMapSplitter, HostsPrisoners}
import pl.edu.agh.ipd.config.IpdConfig

import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

object MasterArenaActor {
  def props(config: IpdConfig): Props =
    Props(classOf[MasterArenaActor], config)

  trait Message extends Serializable
  case object Init extends Message
  case object Stop extends Message
  case object Kill extends Message

  val MasterStatsName = "masterStats"
  def localArenaName(hostId: Int): String =
    s"localArena-$hostId"
}

class MasterArenaActor(config: IpdConfig) extends Actor with ActorLogging {
  import MasterArenaActor._

  private def deployConfig(host: String): Deploy =
    Deploy(scope = RemoteScope(AddressFromURIString(s"akka.tcp://$actorSystemName@$host")))

  val splitter: ArenaMapSplitter = config.mapSplitter.newInstance()
  splitter.init(config.splitterOptions)
  val splitStrategy: HostsPrisoners = {
    val result = splitter.splittingStrategy(config.populationSize, config.availableHosts.size, config.useStubs)
    require(
      result.local.values.reduceLeft(_ ++ _).sorted == (0 until config.populationSize),
      s"Splitter has to use every prisoner id from range [0, ${config.populationSize - 1}] exactly once."
    )
    result
  }

  val masterStatsActor: ActorSelection = context.actorSelection(
    context.actorOf(
      MasterStatsActor.props(
        splitStrategy.local.keys.map(hostId =>
          context.actorSelection(s"${MasterArenaActor.localArenaName(hostId)}/${LocalArenaActor.localStatsName(hostId)}")
        ).toSeq,
        config.topNStrategies, config.populationSize
      ).withDeploy(deployConfig(config.availableHosts.head)),
      MasterArenaActor.MasterStatsName
    ).path
  )

  val localArenas: Map[Int, ActorSelection] =
    splitStrategy.local.map {
      case (hostId, _) =>
        val hostAddress: String = config.availableHosts(hostId)
        log.info(s"Deploying ${MasterArenaActor.localArenaName(hostId)} on $hostAddress")
        val actor = context.actorOf(
          LocalArenaActor.props(hostId, config, context.actorSelection(self.path), masterStatsActor, splitStrategy.local, splitStrategy.stubs(hostId))
            .withDeploy(deployConfig(hostAddress)),
          MasterArenaActor.localArenaName(hostId)
        )
        context.watch(actor)
        (hostId, context.actorSelection(actor.path))
    }

  override def receive: Receive = {
    case Init =>
      localArenas.values.foreach(_ ! LocalArenaActor.Init)

    case Stop =>
      localArenas.values.foreach(_ ! LocalArenaActor.Stop)

    case Kill =>
      localArenas.values.foreach(_ ! Kill)

    case Terminated(ref) =>
      replaceArena(ref)

    case MasterStatsActor.StartStats =>
      localArenas.values.foreach(_ ! MasterStatsActor.StartStats)
  }

  def replaceArena(ref: ActorRef): Unit = {
    val name = ref.path.name
    val hostId = name.stripPrefix("localArena-").toInt
    require(MasterArenaActor.localArenaName(hostId) == name)
    val hostAddress: String = config.availableHosts(hostId)

    log.info(s"Restarting `$name` on $hostAddress")

    context.watch(
      context.actorOf(
        LocalArenaActor.props(hostId, config, context.actorSelection(self.path), masterStatsActor, splitStrategy.local, splitStrategy.stubs(hostId))
          .withDeploy(deployConfig(hostAddress)),
        name
      )
    )
  }
}

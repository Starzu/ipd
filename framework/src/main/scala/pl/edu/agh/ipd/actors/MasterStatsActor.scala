package pl.edu.agh.ipd.actors

import java.util.Date

import akka.actor.{Actor, ActorLogging, ActorSelection, Props}
import akka.event.LoggingReceive
import com.typesafe.scalalogging.LazyLogging
import pl.edu.agh.ipd.model.Strategy.Cooperativity
import pl.edu.agh.ipd.model.StrategyView
import pl.edu.agh.ipd.model.map.EnergyProportionalSamplingMap.StrategyStats

import scala.collection.mutable
import scala.concurrent.duration.DurationLong
import scala.language.postfixOps
import scala.util.Try

object MasterStatsActor {
  sealed trait Message
  case class StrategyInfo(prisonerName: String, energy: PrisonerActor.Energy, energyChangeSinceMutation: PrisonerActor.Energy,
                          fightsSinceMutations: Long, strategy: StrategyView)
  case class LocalStats(epoch: Int, prisoners: Int, allTimeouts: Long, remoteTimeouts: Long,
                        energySum: PrisonerActor.Energy, energyMax: PrisonerActor.Energy, energyMin: PrisonerActor.Energy,
                        allFights: Long, fightsSum: Long, fightsMax: Long, fightsMin: Long,
                        allMutations: Long, mutationsSum: Long, mutationsMax: Long, mutationsMin: Long,
                        remoteFightsStarted: Long, lastCooperationsCount: Long, lastMovesCount: Long, deaths: Long,
                        cooperativitySum: Cooperativity, bestStrategy: Seq[StrategyInfo],
                        prisonersCooperativity: Map[Int, Double],
                        prisonersEnergy: Map[Int, Double]) extends Message

  case object CurrentStats extends Message
  case object ClearStats extends Message
  case object StartStats extends Message
  case object PrisonersCooperativityRequest extends Message
  case object StrategiesRequest extends Message

  def props(localStatsActors: Seq[ActorSelection], topNStrategies: Int, allPrisonersCount: Int): Props =
    Props(classOf[MasterStatsActor], localStatsActors, topNStrategies, allPrisonersCount)

  case class MasterStats(started: Date, prisonersCount: Int,
                         energySum: Double, energyAvg: Double, energyMax: Double, energyMin: Double,
                         allFights: Long, fightsSum: Long, fightsAvg: Long, fightsMax: Long, fightsMin: Long,
                         allTimeouts: Long, remoteTimeouts: Long, remoteFightsStarted: Long,
                         lastCooperationsCount: Long, lastMovesCount: Long, deaths: Long,
                         allMutations: Long, mutationsSum: Long, mutationsAvg: Long, mutationsMax: Long, mutationsMin: Long,
                         cooperativityAvg: Cooperativity, bestStrategies: Seq[StrategyInfo]) {
    lazy val pretty: String = {
      val bestStrategiesString = bestStrategies.map(v =>
        s"${v.energy} - coop: ${v.strategy.cooperativity} - ${v.prisonerName}  (Since last mutation: ${v.energyChangeSinceMutation} energy change, ${v.fightsSinceMutations} fights)\n${v.strategy}"
      ).mkString("\n")

      s"""
         |====================== ${new Date()} ======================
         |Started: $started
         |
       |All energy: $energySum
         |Max. energy per prisoner: $energyMax
         |Avg. energy per prisoner: $energyAvg
         |Min. energy per prisoner: $energyMin
         |
       |All fights: $allFights
         |Max. fights per prisoner: $fightsMax
         |Avg. fights per prisoner: $fightsAvg
         |Min. fights per prisoner: $fightsMin
         |
       |Fight timeouts: $allTimeouts (remote: $remoteTimeouts)
         |Remote fights started: $remoteFightsStarted
         |
       |$lastCooperationsCount cooperations out of $lastMovesCount moves (${Try(100.0 * lastCooperationsCount / lastMovesCount).getOrElse(0.0)}%)
       |Avg. cooperativity: $cooperativityAvg
         |
       |All mutations: $allMutations
         |Max. mutations per prisoner: $mutationsMax
         |Avg. mutations per prisoner: $mutationsAvg
         |Min. mutations per prisoner: $mutationsMin
         |
       |Total deaths: $deaths
         |Max. energy strategies:
         |$bestStrategiesString
         |===========================================================""".stripMargin
    }
  }

}

class MasterStatsActor(localStatsActors: Seq[ActorSelection], topNStrategies: Int, allPrisonersCount: Int) extends Actor with ActorLogging with LazyLogging {
  import MasterStatsActor._

  private var epoch: Int = 0
  private var started: Date = new Date()
  private val localStats = mutable.Map.empty[String, LocalStats]
  private val prisonersCooperativity = mutable.IndexedSeq.fill(allPrisonersCount)(0.5)

  override def receive: Receive = {
    case state: LocalStats if state.epoch == epoch =>
      localStats(sender().path.name) = state
      state.prisonersCooperativity.foreach({ case (ind, coop) => prisonersCooperativity(ind) = coop })
    case CurrentStats =>
      sender() ! stats()
    case ClearStats =>
      epoch += 1
      started = new Date()
      localStats.clear()
      localStatsActors.foreach(_ ! LocalStatsActor.Clear(epoch))
    case PrisonersCooperativityRequest =>
      sender() ! prisonersCooperativity.toIndexedSeq
    case StrategiesRequest =>
      sender() ! localStats.values.flatMap { localStats =>
        localStats.prisonersEnergy
          .map({ case (id, energy) => StrategyStats(id, energy) })
          .toVector
      }.toVector
  }

  private def stats(): MasterStats = {
    val prisonersCount: Int = localStats.values.map(_.prisoners).sum

    val energySum: Double = localStats.values.map(_.energySum).sum
    val energyAvg: Double = Try(energySum / prisonersCount).getOrElse(0)
    val energyMax: Double = localStats.values.map(_.energyMax).maxOrElse(0)
    val energyMin: Double = localStats.values.map(_.energyMin).minOrElse(0)

    val allFights: Long = localStats.values.map(_.allFights).sum
    val fightsSum: Long = localStats.values.map(_.fightsSum).sum / 2
    val fightsAvg: Long = Try(fightsSum * 2 / prisonersCount).getOrElse(0)
    val fightsMax: Long = localStats.values.map(_.fightsMax).maxOrElse(0)
    val fightsMin: Long = localStats.values.map(_.fightsMin).minOrElse(0)

    val allTimeouts: Long = localStats.values.map(_.allTimeouts).sum
    val remoteTimeouts: Long = localStats.values.map(_.remoteTimeouts).sum
    val remoteFightsStarted: Long = localStats.values.map(_.remoteFightsStarted).sum

    val lastCooperationsCount: Long = localStats.values.map(_.lastCooperationsCount).sum
    val lastMovesCount: Long = localStats.values.map(_.lastMovesCount).sum
    val deaths: Long = localStats.values.map(_.deaths).sum

    val allMutations: Long = localStats.values.map(_.allMutations).sum
    val mutationsSum: Long = localStats.values.map(_.mutationsSum).sum
    val mutationsAvg: Long = Try(mutationsSum / prisonersCount).getOrElse(0)
    val mutationsMax: Long = localStats.values.map(_.mutationsMax).maxOrElse(0)
    val mutationsMin: Long = localStats.values.map(_.mutationsMin).minOrElse(0)

    val cooperativityAvg: Cooperativity = Try(localStats.values.map(_.cooperativitySum).sum / prisonersCount).getOrElse(0)

    val bestStrategies: Seq[StrategyInfo] = findTopStrategies()

    MasterStats(started, prisonersCount,
      energySum, energyAvg, energyMax, energyMin,
      allFights, fightsSum, fightsAvg, fightsMax, fightsMin,
      allTimeouts, remoteTimeouts, remoteFightsStarted,
      lastCooperationsCount, lastMovesCount, deaths,
      allMutations, mutationsSum, mutationsAvg, mutationsMax, mutationsMin,
      cooperativityAvg, bestStrategies
    )
  }

  private def findTopStrategies(): Seq[StrategyInfo] =
    localStats.values
      .flatMap(_.bestStrategy)
      .toVector
      .sortBy(_.energy)
      .reverse
      .take(topNStrategies)
}

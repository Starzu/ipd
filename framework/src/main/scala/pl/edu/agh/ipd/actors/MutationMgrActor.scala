package pl.edu.agh.ipd.actors

import akka.actor.{Actor, ActorLogging, ActorSelection, Props}
import pl.edu.agh.ipd.actors.LocalStatsActor.MutationStarted
import pl.edu.agh.ipd.model.{Mutations, StrategyView}

import scala.concurrent.duration.{DurationInt, FiniteDuration}
import scala.language.postfixOps
import scala.util.Random

object MutationMgrActor {
  def props(prisoners: Seq[ActorSelection], stats: ActorSelection, targetFtm: Double, allManagers: Int, replaceEnergy: Option[PrisonerActor.Energy], timeout: FiniteDuration = 5 seconds): Props =
    Props(classOf[MutationMgrActor], prisoners, stats, targetFtm, allManagers, replaceEnergy, timeout)

  sealed trait Message
  case object StartMutation extends Message
}

class MutationMgrActor(val prisoners: Seq[ActorSelection], stats: ActorSelection, targetFtm: Double,
                       allManagers: Int, replaceEnergy: Option[PrisonerActor.Energy], override val timeout: FiniteDuration)
  extends Actor with ActorLogging with ActorTimeout[MutationMgrActor.StartMutation.type] {

  import MutationMgrActor._
  import context.dispatcher

  private var stopped: Boolean = false
  private var mutationsToDo: Long = Long.MaxValue
  override val timeoutMessage = StartMutation

  private var mutationsStarted: Long = 0
  private var startStats: Boolean = false

  context.system.scheduler.schedule(initialDelay(timeout, 0.5), timeout, self, LocalStatsActor.FightsToMutationsRequest)

  override def receive: Receive = {
    case StartMutation =>
      if (!stopped) {
        restartTimeout()
        if (mutationsToDo > 0) {
          val prisoner: ActorSelection = prisoners(Random.nextInt(prisoners.length))
          prisoner ! PrisonerActor.StrategyRequest
        }
      }
    case MasterStatsActor.StartStats =>
      startStats = true
    case LocalArenaActor.Stop =>
      cancelTimeout()
      stopped = true
    case LocalStatsActor.FightsToMutationsRequest =>
      stats ! LocalStatsActor.FightsToMutationsRequest
    case LocalStatsActor.FightsToMutations(fights, mutations) =>
      mutationsToDo = (fights - mutations * targetFtm).toLong / allManagers
    case strategy: StrategyView =>
      sender() ! PrisonerActor.ReplaceStrategy(Mutations.mutate(strategy), replaceEnergy)
      mutationsToDo -= 1
      if (startStats) mutationsStarted += 1
      if (mutationsStarted > 50) {
        stats ! MutationStarted(mutationsStarted)
        mutationsStarted = 0
      }
      continue()
  }
}

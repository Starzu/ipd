package pl.edu.agh.ipd.actors

import akka.actor.{Actor, ActorLogging, ActorSelection, Props}
import pl.edu.agh.ipd.actors.FightMgrActor.MessageWithFightId
import pl.edu.agh.ipd.config.IpdConfig.Payoffs
import pl.edu.agh.ipd.model.Strategy

import scala.collection.mutable
import scala.concurrent.duration.FiniteDuration
import scala.language.postfixOps

object PrisonerActor {
  type Energy = Double

  def props(energy: PrisonerActor.Energy, strategy: Strategy, statsActor: ActorSelection,
            sendStatsInterval: FiniteDuration, fightTimeoutMs: Long, separateStrategies: Boolean): Props =
    Props(classOf[PrisonerActor], energy, strategy, statsActor, sendStatsInterval, fightTimeoutMs, separateStrategies)

  sealed trait Action extends Serializable
  case object Cooperate extends Action
  case object Defect extends Action

  object Action {
    def resolveFight(payoffs: Payoffs)(actionA: Action, actionB: Action): (PrisonerActor.Energy, PrisonerActor.Energy) =
      (actionA, actionB) match {
        case (PrisonerActor.Cooperate, PrisonerActor.Cooperate) =>
          (payoffs.reward, payoffs.reward)
        case (PrisonerActor.Cooperate, PrisonerActor.Defect) =>
          (payoffs.sucker, payoffs.temptation)
        case (PrisonerActor.Defect, PrisonerActor.Cooperate) =>
          (payoffs.temptation, payoffs.sucker)
        case (PrisonerActor.Defect, PrisonerActor.Defect) =>
          (payoffs.punishment, payoffs.punishment)
      }
  }

  trait Message extends Serializable
  case object NextMoveRequest extends Message
  case class FightResult(opponentMove: Action, payoff: Energy) extends Message
  case object StrategyRequest extends Message
  case class ReplaceStrategy(strategy: Strategy, energy: Option[PrisonerActor.Energy]) extends Message
  case class Restart(initEnergy: Energy, strategy: Strategy) extends Message
  case class FightsBatch(results: Seq[FightResult]) extends Message
  case object ForceDeath extends Message

  case object SendStats extends Message

  def prisonerName(id: Int, isStub: Boolean = false): String =
    s"prisoner-${if (isStub) "stub-" else ""}$id"

  def prisonerId(name: String): Int =
    name.split('-').last.toInt
}

class PrisonerActor(private var energy: PrisonerActor.Energy,
                    override protected var _strategy: Strategy,
                    statsActor: ActorSelection,
                    sendStatsInterval: FiniteDuration,
                    override protected val expirationMs: Long,
                    override protected val separateStrategies: Boolean
                   ) extends Actor with ActorLogging with StrategyCache {
  import PrisonerActor._

  import context.dispatcher

  context.system.scheduler.schedule(initialDelay(sendStatsInterval), sendStatsInterval, self, SendStats)

  private var fights = 0
  private val lastMovesCount = 100
  private var lastMoves = mutable.ArrayBuffer.fill[Action](lastMovesCount)(null)
  private var moveInd: Int = 0
  private var mutations = 0

  private var energyChangeSinceMutation: PrisonerActor.Energy = 0
  private var fightsSinceMutation: Int = 0

  private var syncedStubs = mutable.Set.empty[String]

  private var collectStats: Boolean = false

  override def receive: Receive = {
    case MessageWithFightId(id, NextMoveRequest) =>
      val nextMove = strategy(id).nextMove()
      sender() ! MessageWithFightId(id, nextMove)
      lastMoves(moveInd) = nextMove
      moveInd = (moveInd + 1) % lastMovesCount
    case MessageWithFightId(id, res: FightResult) =>
      handle(Some(id))(res)
    case StrategyRequest =>
      sender() ! _strategy.view()
    case ReplaceStrategy(newStrategy, replaceEnergy) =>
      _strategy = newStrategy
      replaceEnergy.foreach(energy = _)
      energyChangeSinceMutation = 0
      fightsSinceMutation = 0
      mutations += 1
      syncedStubs.clear()
    case MasterStatsActor.StartStats =>
      collectStats = true
      fights = 0
      mutations = 0
      lastMoves = lastMoves.transform(_ => null)
    case SendStats if collectStats =>
      statsActor ! LocalStatsActor.PrisonerState(energy, fights, lastMoves.count(_ == Cooperate), lastMoves.count(_ != null), mutations, _strategy.view(), energyChangeSinceMutation, fightsSinceMutation)
      fights = 0
      mutations = 0
    case Restart(e, s) =>
      fights = 0
      mutations = 0
      energyChangeSinceMutation = 0
      fightsSinceMutation = 0
      syncedStubs.clear()
      energy = e
      _strategy = s
      self ! SendStats
    case FightsBatch(results) =>
      val stubPath = sender().path.toString
      if (syncedStubs.contains(stubPath)) {
        results.foreach(handle(None))
      } else {
        sender() ! _strategy.view()
        syncedStubs.add(stubPath)
      }
    case ForceDeath =>
      context.parent ! LocalArenaActor.KillMe
  }

  private def handle(fightId: Option[FightMgrActor.FightId])(fightResult: FightResult): Unit =
    if (collectStats) {
      fightId.foreach(fightId => strategy(fightId).opponentMove(fightResult.opponentMove))
      fights += 1
      fightsSinceMutation += 1
      energy += fightResult.payoff
      energyChangeSinceMutation += fightResult.payoff
      if (energy <= 0 && energy - fightResult.payoff > 0) context.parent ! LocalArenaActor.KillMe
    }
}

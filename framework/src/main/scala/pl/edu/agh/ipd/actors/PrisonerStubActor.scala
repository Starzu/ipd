package pl.edu.agh.ipd.actors

import akka.actor.{Actor, ActorLogging, ActorSelection, Props}
import akka.event.LoggingReceive
import pl.edu.agh.ipd.actors.FightMgrActor.MessageWithFightId
import pl.edu.agh.ipd.actors.PrisonerActor.{FightResult, FightsBatch, StrategyRequest}
import pl.edu.agh.ipd.model.{Strategy, StrategyView}

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration.FiniteDuration
import scala.language.postfixOps

object PrisonerStubActor {
  def props(strategy: Strategy, originalPrisoner: ActorSelection, syncBatchThreshold: Int,
            syncInterval: FiniteDuration, expirationMs: Long, separateStrategies: Boolean): Props =
    Props(classOf[PrisonerStubActor], strategy, originalPrisoner, syncBatchThreshold, syncInterval, expirationMs, separateStrategies)
}

class PrisonerStubActor(override protected var _strategy: Strategy,
                        private val originalActor: ActorSelection,
                        syncBatchThreshold: Int,
                        syncInterval: FiniteDuration,
                        override protected val expirationMs: Long,
                        override protected val separateStrategies: Boolean
                       ) extends Actor with ActorLogging with StrategyCache {
  import context.dispatcher

  private object ForceBatch
  private var buffer = ArrayBuffer.empty[FightResult]

  private var syncTrigger = context.system.scheduler.scheduleOnce(initialDelay(syncInterval, init = 0.1), self, ForceBatch)

  override def receive: Receive = {
    case MessageWithFightId(id, PrisonerActor.NextMoveRequest) =>
      sender() ! MessageWithFightId(id, strategy(id).nextMove())
    case MessageWithFightId(id, res@PrisonerActor.FightResult(opponentMove, _)) =>
      buffer += res
      strategy(id).opponentMove(opponentMove)

      if (buffer.length >= syncBatchThreshold) sendBatch()
    case newStrategy: StrategyView =>
      _strategy = newStrategy.copy()
      buffer.clear()
    case ForceBatch =>
      sendBatch()
    case StrategyRequest =>
      sender() ! _strategy.view()
  }

  private def sendBatch(): Unit = {
    syncTrigger.cancel()
    originalActor ! FightsBatch(buffer.result())
    buffer = ArrayBuffer.empty[FightResult]
    syncTrigger = context.system.scheduler.scheduleOnce(syncInterval, self, ForceBatch)
  }
}

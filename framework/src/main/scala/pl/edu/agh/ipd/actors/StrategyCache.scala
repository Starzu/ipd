package pl.edu.agh.ipd.actors

import java.{lang => jl}
import java.util.concurrent.TimeUnit

import com.google.common.cache.{CacheBuilder, CacheLoader}
import pl.edu.agh.ipd.model.Strategy

trait StrategyCache {
  protected val expirationMs: Long
  protected val separateStrategies: Boolean
  protected var _strategy: Strategy

  private val cache = CacheBuilder.newBuilder()
    .expireAfterAccess(expirationMs, TimeUnit.MILLISECONDS)
    .build(new CacheLoader[jl.Long, Strategy] {
      override def load(key: jl.Long): Strategy = _strategy.copy()
    })

  protected def strategy(fightId: FightMgrActor.FightId): Strategy =
    if (!separateStrategies) _strategy
    else cache.get(fightId.id)
}
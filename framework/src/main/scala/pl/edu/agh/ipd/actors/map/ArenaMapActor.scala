package pl.edu.agh.ipd.actors
package map

import akka.actor.{Actor, ActorLogging, ActorSelection, Props}
import akka.event.LoggingReceive
import pl.edu.agh.ipd.actors.FightMgrActor.MessageWithFightId
import pl.edu.agh.ipd.model.map.NetworkMap

object ArenaMapActor {
  type Neighbourhood = IndexedSeq[ActorSelection]

  def props(cls: Class[_], prisoners: Seq[ActorSelection], mapOptions: Map[String, Any]): Props =
    Props(cls, prisoners, mapOptions)

  trait Message extends Serializable
  case class FindNeighbours(prisonerActor: ActorSelection) extends Message
}

abstract class ArenaMapActor(val prisoners: Seq[ActorSelection]) extends Actor with ActorLogging {
  import ArenaMapActor._

  override def receive: Receive = {
    case MessageWithFightId(id, FindNeighbours(prisonerActor)) =>
      sender() ! MessageWithFightId(id, findNeighbours(prisonerActor))
    case FindNeighbours(prisonerActor) =>
      sender() ! findNeighbours(prisonerActor)
  }

  private val indexes: Map[Int, Int] = prisoners.map(as => PrisonerActor.prisonerId(as.name)).zipWithIndex.toMap
  protected val networkMap: NetworkMap

  protected def findNeighbours(prisonerActor: ActorSelection): Neighbourhood =
    networkMap.findNeighbours(indexes(PrisonerActor.prisonerId(prisonerActor.name)))
      .map(prisoners)
}






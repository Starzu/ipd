package pl.edu.agh.ipd.actors.map

import java.util.concurrent.TimeUnit

import akka.actor.ActorSelection
import akka.util.Timeout
import pl.edu.agh.ipd.actors.MasterStatsActor
import pl.edu.agh.ipd.model.map.EnergyProportionalSamplingMap
import pl.edu.agh.ipd.model.map.EnergyProportionalSamplingMap.{PrisonersEnergyProvider, StrategyStats}

import scala.concurrent.duration.{DurationInt, FiniteDuration}
import scala.language.postfixOps
import scala.util.Success

class EnergyProportionalSamplingMapActor(p: Seq[ActorSelection], val mapOptions: Map[String, Any]) extends ArenaMapActor(p) {
  override protected val networkMap = new EnergyProportionalSamplingMap
  networkMap.init(p.length, mapOptions)
}

object EnergyProportionalSamplingMapActor {
  class ActorPrisonersEnergyProvider(masterStats: ActorSelection, statsRefresh: FiniteDuration) extends PrisonersEnergyProvider {
    import akka.pattern.ask

    private var lastUpdate: Long = 0
    private var lastValue: IndexedSeq[StrategyStats] = IndexedSeq.empty

    override def localStrategies(): IndexedSeq[StrategyStats] = synchronized {
      reload()
      lastValue
    }

    private def reload(): Unit = {
      val now = System.nanoTime()
      if (TimeUnit.NANOSECONDS.toMillis(now - lastUpdate) >= statsRefresh.toMillis) {
        lastUpdate = now
        implicit val timeout = Timeout(5 seconds)
        import scala.concurrent.ExecutionContext.Implicits.global
        masterStats ? MasterStatsActor.StrategiesRequest onComplete {
          case Success(v: IndexedSeq[_]) =>
            synchronized { lastValue = v.asInstanceOf[IndexedSeq[StrategyStats]] }
          case _ =>
            lastUpdate = 0
        }
      }
    }
  }
}
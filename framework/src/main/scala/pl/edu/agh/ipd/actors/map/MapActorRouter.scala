package pl.edu.agh.ipd.actors.map

import akka.actor.{Actor, ActorSelection}
import akka.routing.{ActorRefRoutee, RoundRobinRoutingLogic, Router}
import pl.edu.agh.ipd.actors.FightMgrActor.MessageWithFightId
import pl.edu.agh.ipd.actors.map.ArenaMapActor.FindNeighbours

class MapActorRouter(p: Seq[ActorSelection], val mapOptions: Map[String, Any]) extends Actor {
  require(mapOptions.contains("routees") && mapOptions("routees").isInstanceOf[Int])
  require(mapOptions.contains("routeesClass"))

  private val router = {
    val routeesNum = mapOptions("routees").asInstanceOf[Int]
    val routeesClass = mapOptions("routeesClass").asInstanceOf[String]
    val routees = Vector.fill(routeesNum) {
      val r = context.actorOf(ArenaMapActor.props(Class.forName(routeesClass), p, mapOptions))
      ActorRefRoutee(r)
    }
    Router(RoundRobinRoutingLogic(), routees)
  }

  override def receive: Receive = {
    case m: MessageWithFightId =>
      router.route(m, sender())
    case m: FindNeighbours =>
      router.route(m, sender())
  }
}

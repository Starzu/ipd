package pl.edu.agh.ipd.actors
package map

import akka.actor.ActorSelection
import pl.edu.agh.ipd.model.map.RandomNetwork

class RandomArenaMapActor(p: Seq[ActorSelection], val mapOptions: Map[String, Any]) extends ArenaMapActor(p) {
  override protected val networkMap = new RandomNetwork
  networkMap.init(p.length, mapOptions)
}

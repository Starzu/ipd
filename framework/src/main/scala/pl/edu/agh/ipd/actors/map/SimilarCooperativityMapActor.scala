package pl.edu.agh.ipd.actors.map

import java.util.concurrent.TimeUnit

import akka.actor.ActorSelection
import akka.util.Timeout
import pl.edu.agh.ipd.actors.MasterStatsActor
import pl.edu.agh.ipd.model.Strategy.Cooperativity
import pl.edu.agh.ipd.model.map.SimilarCooperativityNetwork

import scala.concurrent.duration.{DurationDouble, FiniteDuration}
import scala.language.postfixOps
import scala.util.Success

class SimilarCooperativityMapActor(p: Seq[ActorSelection], val mapOptions: Map[String, Any]) extends ArenaMapActor(p) {
  override protected val networkMap = new SimilarCooperativityNetwork
  networkMap.init(p.length, mapOptions)
}

object SimilarCooperativityMapActor {
  class ActorCooperativityProvider(masterStats: ActorSelection, statsRefresh: FiniteDuration, prisonersCount: Int) extends SimilarCooperativityNetwork.CooperativityProvider {
    import akka.pattern.ask

    private var lastUpdate: Long = 0
    private var lastValue: IndexedSeq[Cooperativity] = IndexedSeq.fill(prisonersCount)(0.5)

    override def prisonersCooperativity(): IndexedSeq[Cooperativity] = synchronized {
      reload()
      lastValue
    }

    private def reload(): Unit = {
      val now = System.nanoTime()
      if (TimeUnit.NANOSECONDS.toMillis(now - lastUpdate) >= statsRefresh.toMillis) {
        lastUpdate = now
        implicit val timeout = Timeout(5 seconds)
        import scala.concurrent.ExecutionContext.Implicits.global
        masterStats ? MasterStatsActor.PrisonersCooperativityRequest onComplete {
          case Success(v: IndexedSeq[_]) =>
            synchronized { lastValue = v.asInstanceOf[IndexedSeq[Cooperativity]] }
          case _ =>
            lastUpdate = 0
        }
      }
    }
  }
}
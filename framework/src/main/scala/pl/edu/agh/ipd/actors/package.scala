package pl.edu.agh.ipd

import akka.actor.ActorSelection

import scala.concurrent.duration.{DurationLong, FiniteDuration}
import scala.language.postfixOps
import scala.util.Random

package object actors {
  val actorSystemName: String = "ipd"

  implicit class ActorSelectionExt(val sel: ActorSelection) extends AnyVal {
    def name: String =
      sel.pathString.split("/").last
  }

  implicit class TraversableOnceExt[T](val traversable: TraversableOnce[T]) extends AnyVal {
    def maxOrElse(default: => T)(implicit cmp: Ordering[T]): T =
      if (traversable.isEmpty) default
      else traversable.max

    def minOrElse(default: => T)(implicit cmp: Ordering[T]): T =
      if (traversable.isEmpty) default
      else traversable.min
  }

  def initialDelay(interval: FiniteDuration, init: Double = 1): FiniteDuration =
    ((interval.toMillis * init).toLong + Random.nextLong() % interval.toMillis) millis
}

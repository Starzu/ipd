package pl.edu.agh.ipd.actors.splitter

case class HostsPrisoners(local: Map[Int, Seq[Int]], stubs: Map[Int, Seq[Int]])

trait ArenaMapSplitter {
  def init(splitterOptions: Map[String, Any]): Unit = {}

  /** Returns splitting strategy for `prisonersCount` prisoners over `hosts` JVMs.
    * Eg.:
    * Map(
    *   0 -> Seq(0, 1, 2), // first JVM
    *   1 -> Seq(3, 4, 5), // second
    *   2 -> Seq(6, 7, 8)  // third
    * )
    */
  def splittingStrategy(prisonersCount: Int, hosts: Int, useStubs: Boolean): HostsPrisoners
}


package pl.edu.agh.ipd.actors.splitter

import pl.edu.agh.ipd.utils.GraphGeneration

import scala.collection.mutable

class GraphMapSplitter extends ArenaMapSplitter {
  private var neighbors: Map[Int, Set[Int]] = _

  override def init(splitterOptions: Map[String, Any]): Unit = {
    require(splitterOptions.contains("filename") && splitterOptions("filename").isInstanceOf[String])
    neighbors = GraphGeneration.read(splitterOptions("filename").asInstanceOf[String])
  }

  override def splittingStrategy(prisonersCount: Int, hosts: Int, useStubs: Boolean): HostsPrisoners = {
    val local = (0 until hosts).map(idx => (idx, mutable.Set[Int]())).toMap
    val stubs = (0 until hosts).map(idx => (idx, mutable.Set[Int]())).toMap
    val degrees = neighbors.mapValues(_.size)
    val visited = mutable.ArrayBuffer.fill(prisonersCount)(false)
    val nodesQueue = mutable.Queue[(Int, Int)](degrees.toSeq.sortBy(-_._2).take(hosts).map(_._1).reverse.zipWithIndex:_*)
    while (nodesQueue.nonEmpty) {
      val (node, partition) = nodesQueue.dequeue()
      if (!visited(node)) {
        visited(node) = true
        local(partition) += node
        nodesQueue.enqueue(neighbors(node).toSeq.map(v => (v, partition)):_*)
      } else if (useStubs && !local(partition).contains(node)) {
        stubs(partition) += node
      }
    }

    HostsPrisoners(local.mapValues(_.result().toVector).map(identity), stubs.mapValues(_.result().toVector).map(identity))
  }
}

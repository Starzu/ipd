package pl.edu.agh.ipd.actors.splitter

class OneDimArenaMapSplitter extends ArenaMapSplitter {
  override def splittingStrategy(prisonersCount: Int, hosts: Int, useStubs: Boolean): HostsPrisoners = {
    val localPrisonersPerNode = (0 until prisonersCount)
      .map(id => (hostIdForPrisoner(id, prisonersCount, hosts), id))
      .groupBy(_._1)
      .map({ case (key, value) => (key, value.map(_._2)) })

    val stubsPerNode: Map[Int, Seq[Int]] = List.tabulate(hosts)(i => {
      val stubs =
        if (useStubs) Seq(localPrisonersPerNode(i).min - 1, localPrisonersPerNode(i).max + 1).filter(v => v >= 0 && v < prisonersCount)
        else List.empty[Int]
      (i, stubs)
    }).toMap

    HostsPrisoners(localPrisonersPerNode, stubsPerNode)
  }

  private def hostIdForPrisoner(prisonerId: Int, prisonersCount: Int, hosts: Int): Int = {
    val groupSize = prisonersCount / hosts + (if (prisonersCount % hosts != 0) 1 else 0)
    prisonerId / groupSize
  }
}

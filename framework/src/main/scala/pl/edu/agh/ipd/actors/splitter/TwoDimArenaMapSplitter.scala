package pl.edu.agh.ipd.actors.splitter

import pl.edu.agh.ipd.model.map.TwoDimNetwork

class TwoDimArenaMapSplitter extends ArenaMapSplitter {
  private var width: Int = 0

  override def init(splitterOptions: Map[String, Any]): Unit = {
    require(splitterOptions.contains("width") && splitterOptions("width").isInstanceOf[Int])
    width = splitterOptions("width").asInstanceOf[Int]
  }

  override def splittingStrategy(prisonersCount: Int, hosts: Int, useStubs: Boolean): HostsPrisoners = {
    val localPrisonersPerNode =
      if (width < prisonersCount.toDouble / width) {
        val rows = prisonersCount / width + (if (prisonersCount % width != 0) 1 else 0)
        val rowsPerHost = rows / hosts + (if (rows % hosts != 0) 1 else 0)
        (0 until hosts)
          .map(id => (id, ((id * width * rowsPerHost) until Math.min((id + 1) * width * rowsPerHost, prisonersCount)).toVector))
          .toMap
      } else {
        val colsPerHost = width / hosts + (if (width % hosts != 0) 1 else 0)
        (0 until hosts)
          .map(id => (id, (id * colsPerHost until (id + 1) * colsPerHost).flatMap(s => s.until(prisonersCount, width).toVector)))
          .toMap
      }

    val network = new TwoDimNetwork
    network.init(prisonersCount, Map("width" -> width))
    val stubsPerNode: Map[Int, Seq[Int]] = List.tabulate(hosts)(i => {
      val stubs =
        if (useStubs) localPrisonersPerNode(i).flatMap(network.findNeighbours).distinct.filter(n => !localPrisonersPerNode(i).contains(n))
        else List.empty[Int]
      (i, stubs)
    }).toMap

    HostsPrisoners(localPrisonersPerNode, stubsPerNode)
  }
}

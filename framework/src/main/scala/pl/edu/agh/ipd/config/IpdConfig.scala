package pl.edu.agh.ipd.config

import pl.edu.agh.ipd.actors.splitter.ArenaMapSplitter
import pl.edu.agh.ipd.actors.PrisonerActor
import pl.edu.agh.ipd.config.IpdConfig.Payoffs
import pl.edu.agh.ipd.model.{Strategies, Strategy}

case class IpdConfig(populationSize: Int,
                     private val _initialStrategy: String,
                     separateStrategies: Boolean,
                     initialEnergy: PrisonerActor.Energy,
                     payoffs: Payoffs,
                     mapSplitterName: String,
                     splitterOptions: Map[String, Any],
                     useStubs: Boolean,
                     mapName: String,
                     mapOptions: Map[String, Any],
                     crossoverMapName: String,
                     crossoverMapOptions: Map[String, Any],
                     targetFightsToMutationsRatio: Double,
                     crossoverMgrsCount: Int,
                     fightMgrsCount: Int,
                     mutationMgrsCount: Int,
                     replaceEnergyOnMutation: Boolean,
                     forceEpochDeaths: Boolean,
                     fightTimeoutMs: Int,
                     continuationProbability: Double,
                     stubSyncIntervalMs: Int,
                     stubSyncBatchThreshold: Int,
                     localStatsIntervalMs: Int,
                     masterStatsIntervalMs: Int,
                     topNStrategies: Int,
                     availableHosts: Seq[String]) {

  def mapSplitter: Class[ArenaMapSplitter] = Class.forName(mapSplitterName).asInstanceOf[Class[ArenaMapSplitter]]
  def map: Class[_] = Class.forName(mapName)
  def crossoverMap: Class[_] = Class.forName(crossoverMapName)
  def initialStrategy(): Strategy = Strategies.getStrategy(_initialStrategy).copy()

  def validate(): Unit = {
    require(populationSize > fightMgrsCount)
    require(populationSize > mutationMgrsCount)
    require(fightTimeoutMs > 0)
  }
}

object IpdConfig {
  import PrisonerActor._

  case class Payoffs(temptation: Energy, reward: Energy, punishment: Energy, sucker: Energy)
}

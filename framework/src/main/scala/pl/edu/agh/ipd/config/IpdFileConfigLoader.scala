package pl.edu.agh.ipd.config

import java.io.File

import com.typesafe.config.ConfigFactory
import pl.edu.agh.ipd.actors.PrisonerActor
import pl.edu.agh.ipd.model.{Strategies, Strategy}

object IpdFileConfigLoader {
  import scala.collection.JavaConverters._
  import IpdConfig._

  def get(filename: String): IpdConfig = {
    val config = {
      val f = new File(filename)
      if (f.exists()) ConfigFactory.parseFile(f)
      else ConfigFactory.load(filename)
    }
    val ipdConfig = config.getObject("ipd").toConfig

    def options(key: String) = {
      (for (
        entry <- ipdConfig.getObject(key).entrySet().asScala;
        key = entry.getKey;
        value = entry.getValue.unwrapped()
      ) yield (key, value)).toMap
    }

    val populationSize: Int =
      ipdConfig.getInt("populationSize")

    def initialStrategy: String =
      ipdConfig.getString("initialStrategy")

    val separateStrategies: Boolean =
      ipdConfig.getBoolean("separateStrategies")

    val initialEnergy: PrisonerActor.Energy =
      ipdConfig.getDouble("initialEnergy")

    val payoffs: Payoffs =
      Payoffs(
        ipdConfig.getDouble("temptation"),
        ipdConfig.getDouble("reward"),
        ipdConfig.getDouble("punishment"),
        ipdConfig.getDouble("sucker")
      )

    val mapSplitter: String =
      ipdConfig.getString("mapSplitter")

    val splitterOptions: Map[String, Any] =
      options("splitterOptions")

    val map: String =
      ipdConfig.getString("map")

    val mapOptions: Map[String, Any] =
      options("mapOptions")

    val crossoverMap: String =
      ipdConfig.getString("crossoverMap")

    val crossoverMapOptions: Map[String, Any] =
      options("crossoverMapOptions")

    val useStubs = ipdConfig.getBoolean("useStubs")

    val targetFightsToMutationsRatio: Double =
      ipdConfig.getDouble("targetFightsToMutationsRatio")

    val crossoverMgrsCount: Int =
      ipdConfig.getInt("crossoverMgrsCount")

    val fightMgrsCount: Int =
      ipdConfig.getInt("fightMgrsCount")

    val mutationMgrsCount: Int =
      ipdConfig.getInt("mutationMgrsCount")

    val replaceEnergyOnMutation: Boolean =
      ipdConfig.getBoolean("replaceEnergyOnMutation")

    val forceEpochDeaths: Boolean =
      ipdConfig.getBoolean("forceEpochDeaths")

    val fightTimeoutMs: Int =
      ipdConfig.getInt("fightTimeoutMs")

    val continuationProbability: Double =
      ipdConfig.getDouble("continuationProbability")

    val stubSyncIntervalMs: Int =
      ipdConfig.getInt("stubSyncIntervalMs")

    val stubSyncBatchThreshold: Int =
      ipdConfig.getInt("stubSyncBatchThreshold")

    val localStatsIntervalMs: Int =
      ipdConfig.getInt("localStatsIntervalMs")

    val masterStatsIntervalMs: Int =
      ipdConfig.getInt("masterStatsIntervalMs")

    val topNStrategies: Int =
      ipdConfig.getInt("topNStrategies")

    val availableHosts: Seq[String] =
      ipdConfig.getStringList("availableHosts").asScala

    IpdConfig(
      populationSize, initialStrategy, separateStrategies, initialEnergy, payoffs, mapSplitter, splitterOptions, useStubs,
      map, mapOptions, crossoverMap, crossoverMapOptions, targetFightsToMutationsRatio,
      fightMgrsCount, crossoverMgrsCount, mutationMgrsCount, replaceEnergyOnMutation, forceEpochDeaths, fightTimeoutMs, continuationProbability,
      stubSyncIntervalMs, stubSyncBatchThreshold, localStatsIntervalMs, masterStatsIntervalMs, topNStrategies, availableHosts
    )
  }
}
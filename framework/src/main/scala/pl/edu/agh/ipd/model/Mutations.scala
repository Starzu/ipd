package pl.edu.agh.ipd.model

import com.typesafe.scalalogging.LazyLogging
import pl.edu.agh.ipd.actors.PrisonerActor.{Cooperate, Defect}
import pl.edu.agh.ipd.model.Strategy.{State, Transition}

import scala.collection.mutable
import scala.util.Random

object Mutations extends LazyLogging {
  def mutate(strategyView: StrategyView): Strategy =
    Random.nextInt(4) match {
      case 0 => addState(strategyView)
      case 1 => removeRandomState(strategyView)
      case 2 => changeActionInRandomState(strategyView)
      case 3 => changeTransitionFromRandomState(strategyView)
    }

  def addState(strategyView: StrategyView): Strategy = {
    val states = strategyView.transitions.keys.toBuffer
    val transitionsBuffer = strategyView.transitions.toBuffer

    val newState: State = if (Random.nextBoolean()) new State(Cooperate) else new State(Defect)
    states.append(newState)

    //make random arrow point to new state
    val randomInd: Int = Random.nextInt(transitionsBuffer.length)
    transitionsBuffer(randomInd) = {
      val randomTransition: Transition = transitionsBuffer(randomInd)._2
      val (afterCooperation, afterDefection) =
        if (Random.nextBoolean()) (newState, randomTransition.afterDefection)
        else (randomTransition.afterCooperation, newState)
      val newTransition = randomTransition.copy(afterCooperation = afterCooperation, afterDefection = afterDefection)
      transitionsBuffer(randomInd).copy(_2 = newTransition)
    }

    //make transition for new state
    transitionsBuffer.append(
      (newState, Transition(
        states(Random.nextInt(states.length)),
        states(Random.nextInt(states.length))
      ))
    )

    new Strategy(transitionsBuffer.toMap, strategyView.initState)
  }

  def removeRandomState(strategyView: StrategyView): Strategy =
    removeState(strategyView, strategyView.transitions.keys.toSeq(Random.nextInt(strategyView.transitions.size)))

  def removeState(strategyView: StrategyView, state: State): Strategy = {
    val states: mutable.Buffer[State] = strategyView.transitions.keys.toBuffer
    states.length match {
      case 1 =>
        logger.debug("Mutation: Cannot delete state from one-state strategy, copying strategy")
        strategyView.copy()
      case _ =>
        val transitionsBuffer = strategyView.transitions.toBuffer

        val removedState: State = states.remove(states.indexOf(state))
        val newInitState: State =
          if (strategyView.initState == removedState) {
            val possibleNewInitStates: Seq[State] = Seq(
              strategyView.transitions(strategyView.initState).nextState(Cooperate),
              strategyView.transitions(strategyView.initState).nextState(Defect)
            ).filter(_ != strategyView.initState)
            //init state has transition to at least one state other than itself because there are at least two states
            possibleNewInitStates(Random.nextInt(possibleNewInitStates.length))
          }
          else strategyView.initState

        val newTransitions = transitionsBuffer
          .filter(_._1 != removedState)
          .map {
            case (state, transition) =>
              val afterCooperation: State =
                if (transition.afterCooperation == removedState) states(Random.nextInt(states.length))
                else transition.afterCooperation
              val afterDefection: State =
                if (transition.afterDefection == removedState) states(Random.nextInt(states.length))
                else transition.afterDefection
              (state, transition.copy(afterCooperation = afterCooperation, afterDefection = afterDefection))
          }
        new Strategy(newTransitions.toMap, newInitState)
    }
  }

  def changeActionInRandomState(strategyView: StrategyView): Strategy =
    changeActionInState(strategyView, strategyView.transitions.keys.toSeq(Random.nextInt(strategyView.transitions.size)))

  def changeActionInState(strategyView: StrategyView, state: State): Strategy = {
    val transitions: mutable.Buffer[(State, Transition)] = strategyView.transitions.toBuffer
    val changedState: State = state
    val newState: State = changedState.move match {
      case Cooperate => new State(Defect)
      case Defect => new State(Cooperate)
    }

    val newTransitions: mutable.Buffer[(State, Transition)] = transitions map {
      case (_state, _transition) =>
        val state = if (_state == changedState) newState else _state
        val afterCooperation = if (_transition.afterCooperation == changedState) newState else _transition.afterCooperation
        val afterDefection = if (_transition.afterDefection == changedState) newState else _transition.afterDefection
        (state, Transition(afterCooperation = afterCooperation, afterDefection = afterDefection))
    }
    val newInitState = if (strategyView.initState == changedState) newState else newTransitions(transitions.map(_._1).indexOf(strategyView.initState))._1
    new Strategy(newTransitions.toMap, newInitState)
  }

  def changeTransitionFromRandomState(strategyView: StrategyView): Strategy =
    changeTransitionFromState(strategyView, strategyView.transitions.keys.toSeq(Random.nextInt(strategyView.transitions.size)))

  def changeTransitionFromState(strategyView: StrategyView, state: State): Strategy = {
    val transitions: mutable.Buffer[(State, Transition)] = strategyView.transitions.toBuffer
    transitions.length match {
      case 1 =>
        logger.debug("Mutation: Cannot change transition for one-state strategy, copying strategy")
        strategyView.copy()
      case _ =>
        def randomOtherState(state: State) = {
          val otherStates: Seq[State] = strategyView.transitions.keys.filter(_ != state).toSeq
          otherStates(Random.nextInt(otherStates.length))
        }

        val ind: Int = transitions.map(_._1).indexOf(state)
        transitions(ind) = {
          val transition: Transition = transitions(ind)._2
          val (afterCooperation, afterDefection) =
            if (Random.nextBoolean()) (randomOtherState(transition.nextState(Cooperate)), transition.nextState(Defect))
            else (transition.nextState(Cooperate), randomOtherState(transition.nextState(Defect)))
          transitions(ind).copy(_2 = Transition(afterCooperation = afterCooperation, afterDefection = afterDefection))
        }
        new Strategy(transitions.toMap, strategyView.initState)
    }
  }

}

package pl.edu.agh.ipd.model

import pl.edu.agh.ipd.actors.PrisonerActor.{Action, Cooperate, Defect}
import pl.edu.agh.ipd.model.Strategy._

import scala.util.Random

object Strategies {
  object InitStrategy extends Enumeration {
    val TitForTat, AllCooperate, AllDefect, Random = Value
  }

  def getStrategy(strategyName: String) = InitStrategy.withName(strategyName) match {
    case InitStrategy.TitForTat => titForTat
    case InitStrategy.AllCooperate => allCooperate
    case InitStrategy.AllDefect => allDefect
    case InitStrategy.Random => random
  }

  def titForTat: Strategy = {
    val coopState = new State(Cooperate)
    val defState = new State(Defect)
    val coopTransition = Transition(coopState, defState)
    val defTransition = Transition(coopState, defState)
    new Strategy(Map(coopState -> coopTransition, defState -> defTransition), coopState)
  }

  def suspiciousTitForTat: Strategy = {
    val coopState = new State(Cooperate)
    val defState = new State(Defect)
    val coopTransition = Transition(coopState, defState)
    val defTransition = Transition(coopState, defState)
    new Strategy(Map(coopState -> coopTransition, defState -> defTransition), defState)
  }

  def grim: Strategy = {
    val coopState = new State(Cooperate)
    val defState = new State(Defect)
    val coopTransition = Transition(coopState, defState)
    val defTransition = Transition(defState, defState)
    new Strategy(Map(coopState -> coopTransition, defState -> defTransition), coopState)
  }

  def constStrategy(action: Action): Strategy = {
    val soleState = new State(action)
    new Strategy(Map(soleState -> Transition(soleState, soleState)), soleState)
  }

  def allCooperate: Strategy =
    constStrategy(Cooperate)

  def allDefect: Strategy =
    constStrategy(Defect)

  def random: Strategy = {
    var s = if (Random.nextBoolean()) allCooperate else allDefect
    for (_ <- 1 to 20)
      s = Mutations.mutate(s)
    s
  }
}

package pl.edu.agh.ipd.model

import pl.edu.agh.ipd.actors.PrisonerActor

import scala.collection.mutable
import scala.util.Random

trait StrategyView extends Serializable {
  import Strategy._

  val transitions: Map[State, Transition]
  val initState: State

  /* Returns new strategy based on the strategy view */
  def copy(initState: State = this.initState, transitions: Map[State, Transition] = this.transitions): Strategy

  override def toString: String = {
    val statesInd = transitions.keys.zipWithIndex.toMap
    val transitionsStr: Seq[String] = transitions.toSeq.flatMap({
      case (state, transition) => Seq(
        s"s${statesInd(state)} - ${state.move} - C->s${statesInd(transition.afterCooperation)} - D->s${statesInd(transition.afterDefection)}"
      )
    })
    (s"\tinit state: s${statesInd(initState)}" +: transitionsStr).mkString("\n\t")
  }

  lazy val cooperativity: Cooperativity = {
    val fights = 20
    val strategy = copy()

    val cooperations: Double = (0 until fights).count { _ =>
      val move = strategy.nextMove()
      strategy.opponentMove(move)
      move == PrisonerActor.Cooperate
    }
    cooperations / fights
  }
}

object Strategy {
  type Cooperativity = Double
  class State(val move: PrisonerActor.Action)

  case class Transition(afterCooperation: State, afterDefection: State) {
    import pl.edu.agh.ipd.actors.PrisonerActor._

    def nextState(action: Action): State = action match {
      case Cooperate => afterCooperation
      case Defect => afterDefection
    }
  }

  //TODO implement
  def crossover(strategyA: StrategyView, strategyB: StrategyView): Strategy =
    if (Random.nextBoolean()) new Strategy(strategyA.transitions, strategyA.initState)
    else new Strategy(strategyB.transitions, strategyB.initState)
}

class Strategy(_transitions: Map[Strategy.State, Strategy.Transition], override val initState: Strategy.State, minimize: Boolean = true) extends StrategyView {
  import pl.edu.agh.ipd.actors.PrisonerActor._
  import Strategy._

  if (minimize) require(_transitions.contains(initState))

  override val transitions: Map[State, Transition] = if (minimize) {
    val visited = mutable.Set.empty[State]
    def visit(state: State): Unit =
      if (!visited.contains(state)) {
        visited += state
        visit(_transitions(state).afterCooperation)
        visit(_transitions(state).afterDefection)
      }
    visit(initState)

    _transitions.filterKeys(visited.contains)
  } else _transitions
  private var currentState: State = initState

  /* Returns the next move from strategy. */
  def nextMove(): Action =
    currentState.move

  /* Modifies strategy basing on the opponent move. */
  def opponentMove(action: Action): Unit =
    currentState = transitions(currentState).nextState(action)

  /* Returns an immutable view of the strategy state. */
  def view(): StrategyView =
    this

  override def copy(initState: State = this.initState, transitions: Map[State, Transition] = this.transitions): Strategy =
    new Strategy(transitions, initState, transitions != this.transitions || initState != this.initState)
}
package pl.edu.agh.ipd.model.map

import com.typesafe.scalalogging.StrictLogging
import pl.edu.agh.ipd.actors.PrisonerActor
import pl.edu.agh.ipd.model.StrategyView

import scala.collection.mutable
import scala.util.Random

object EnergyProportionalSamplingMap {
  case class StrategyStats(prisoner: Int, energy: PrisonerActor.Energy)
  trait PrisonersEnergyProvider {
    def localStrategies(): IndexedSeq[StrategyStats]
  }
}

class EnergyProportionalSamplingMap extends NetworkMap with StrictLogging {
  import pl.edu.agh.ipd.model.map.EnergyProportionalSamplingMap._

  private var prisonersCount: Int = _
  private var localStrategiesProvider: PrisonersEnergyProvider = _

  override def init(prisonersCount: Int, options: Map[String, Any]): Unit = {
    require(options.contains("localStrategiesProvider") && options("localStrategiesProvider").isInstanceOf[PrisonersEnergyProvider])
    localStrategiesProvider = options("localStrategiesProvider").asInstanceOf[PrisonersEnergyProvider]
    this.prisonersCount = prisonersCount
  }

  override def findNeighbours(prisoner: Int): IndexedSeq[Int] = {
    val allLocalStrategies = localStrategiesProvider.localStrategies()
    if (allLocalStrategies.isEmpty) {
      logger.warn("Missing strategies stats, returning random")
      Vector(Random.nextInt(prisonersCount))
    } else {
      Vector(sample(allLocalStrategies))
    }
  }

  private def sample(samples: IndexedSeq[StrategyStats]): Int = {
    val min = samples.map(_.energy).min - 1
    val energySum = samples.map(v => v.energy - min).sum.toLong
    val random = Random.nextLong() % energySum
    var energyAcc = 0.0
    var samplesInd = 0
    while (random > energyAcc + samples(samplesInd).energy) {
      energyAcc += samples(samplesInd).energy - min
      samplesInd += 1
    }
    samples(samplesInd).prisoner
  }
}

package pl.edu.agh.ipd.model.map

import pl.edu.agh.ipd.utils.GraphGeneration

import scala.collection.mutable
import scala.util.Random

trait NetworkMap {
  def init(prisonersCount: Int, options: Map[String, Any]): Unit
  def findNeighbours(prisoner: Int): IndexedSeq[Int]
}

class OneDimNetwork extends NetworkMap {
  private var prisonersCount: Int = _
  override def init(pc: Int, options: Map[String, Any]): Unit = {
    prisonersCount = pc
  }

  override def findNeighbours(prisoner: Int): IndexedSeq[Int] = prisoner match {
    case 0 => Vector(1)
    case n if n == prisonersCount - 1 => Vector(prisonersCount - 2)
    case n if n > 0 => Vector(n - 1, n + 1)
  }
}

class TwoDimNetwork extends NetworkMap {
  private var prisonersCount: Int = _
  private var width: Int = _
  private val cache: mutable.Map[Int, IndexedSeq[Int]] = mutable.Map.empty

  override def init(pc: Int, options: Map[String, Any]): Unit = {
    require(options.contains("width") && options("width").isInstanceOf[Int])
    prisonersCount = pc
    width = options("width").asInstanceOf[Int]
  }

  override def findNeighbours(prisoner: Int): IndexedSeq[Int] = {
    def oneToTwo(index: Int) = (index / width, index % width)
    def twoToOne(coords: (Int, Int)) = coords._1 * width + coords._2

    cache.getOrElseUpdate(prisoner, {
      val coords = oneToTwo(prisoner)
      Stream(
        coords.copy(_1 = coords._1 - 1),
        coords.copy(_1 = coords._1 + 1),
        coords.copy(_2 = coords._2 - 1),
        coords.copy(_2 = coords._2 + 1)
      ).filter({
        case (x, y) if x >= 0 && y >= 0 && y < width => true
        case _ => false
      }).map(twoToOne).filter(_ < prisonersCount).toIndexedSeq
    })
  }
}

class RandomNetwork extends NetworkMap {
  var prisonersCount: Int = _
  override def init(pc: Int, options: Map[String, Any]): Unit = {
    prisonersCount = pc
  }

  override def findNeighbours(prisoner: Int): IndexedSeq[Int] = {
    var idx = Random.nextInt(prisonersCount)
    while (idx == prisoner) idx = Random.nextInt(prisonersCount)
    Vector(idx)
  }
}

class GraphNetwork extends NetworkMap {
  private var prisonersCount: Int = _
  private var neighbors: Map[Int, Set[Int]] = _
  override def init(pc: Int, options: Map[String, Any]): Unit = {
    require(options.contains("filename") && options("filename").isInstanceOf[String])
    prisonersCount = pc
    neighbors = GraphGeneration.read(options("filename").asInstanceOf[String])
  }

  override def findNeighbours(prisoner: Int): IndexedSeq[Int] = {
    neighbors(prisoner).toVector
  }
}
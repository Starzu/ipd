package pl.edu.agh.ipd.sequential

import pl.edu.agh.ipd.actors.PrisonerActor
import pl.edu.agh.ipd.actors.PrisonerActor.{Cooperate, Defect}
import pl.edu.agh.ipd.config.IpdConfig.Payoffs
import pl.edu.agh.ipd.model.Strategy.{State, Transition}
import pl.edu.agh.ipd.model.{Strategies, Strategy}

import scala.language.postfixOps

object OneVsOne {
  def s149d: Strategy = {
    val s1 = new State(Cooperate)
    val s2 = new State(Defect)
    val s3 = new State(Cooperate)
    val s4 = new State(Cooperate)
    new Strategy(Map(
      s1 -> Transition(s2, s3),
      s2 -> Transition(s3, s2),
      s3 -> Transition(s4, s2),
      s4 -> Transition(s1, s3)
    ), s1)
  }

  def s149d_n1d: Strategy = {
    val s1 = new State(Defect)
    val s2 = new State(Cooperate)
    val s3 = new State(Defect)
    new Strategy(Map(
      s1 -> Transition(s2, s3),
      s2 -> Transition(s1, s1),
      s3 -> Transition(s1, s2)
    ), s1)
  }

  def s149d_n2d: Strategy = {
    val s1 = new State(Cooperate)
    val s2 = new State(Defect)
    val s3 = new State(Defect)
    val s4 = new State(Cooperate)
    val s5 = new State(Defect)
    val s6 = new State(Defect)
    new Strategy(Map(
      s1 -> Transition(s2, s6),
      s2 -> Transition(s1, s1),
      s3 -> Transition(s6, s2),
      s4 -> Transition(s6, s6),
      s5 -> Transition(s3, s2),
      s6 -> Transition(s4, s5)
    ), s6)
  }

  def s23d: Strategy = {
    val s1 = new State(Defect)
    val s2 = new State(Defect)
    val s3 = new State(Cooperate)
    new Strategy(Map(
      s1 -> Transition(s2, s1),
      s2 -> Transition(s2, s3),
      s3 -> Transition(s3, s2)
    ), s1)
  }

  def s23d_n1d: Strategy = {
    val s1 = new State(Defect)
    val s2 = new State(Defect)
    val s3 = new State(Cooperate)
    val s4 = new State(Cooperate)
    new Strategy(Map(
      s1 -> Transition(s4, s1),
      s2 -> Transition(s2, s3),
      s3 -> Transition(s1, s1),
      s4 -> Transition(s2, s2)
    ), s3)
  }

  def s23d_n39d: Strategy = {
    val s1 = new State(Defect)
    val s2 = new State(Cooperate)
    val s3 = new State(Defect)
    val s4 = new State(Defect)
    val s5 = new State(Defect)
    new Strategy(Map(
      s1 -> Transition(s4, s1),
      s2 -> Transition(s4, s5),
      s3 -> Transition(s2, s4),
      s4 -> Transition(s1, s5),
      s5 -> Transition(s2, s3)
    ), s2)
  }

  def main(args: Array[String]): Unit = {
    val payoffs = Payoffs(2, 1, -1, -2)

    for (neighbourStrategy <- Seq(
      Strategies.allCooperate, Strategies.allDefect, Strategies.titForTat, Strategies.suspiciousTitForTat,
      Strategies.grim, s149d_n1d, s149d_n2d, s23d_n1d, s23d_n39d
    )) {
      val prisonerA = new Prisoner(0, 0, s23d)
      val prisonerB = new Prisoner(0, 0, neighbourStrategy)

      for (_ <- 1 to 20) {
        val moveA = prisonerA.nextAction()
        val moveB = prisonerB.nextAction()

        val (payoffA, payoffB) = PrisonerActor.Action.resolveFight(payoffs)(moveA, moveB)
        prisonerA.payoff(payoffA, moveB)
        prisonerB.payoff(payoffB, moveA)

        println(moveA, moveB, prisonerA.energy, prisonerB.energy)
      }

      println(prisonerA.energy, prisonerB.energy)
      println()
      println()
    }
  }
}

package pl.edu.agh.ipd.sequential

import java.util.{Date, UUID}
import java.util.concurrent.TimeUnit

import pl.edu.agh.ipd.actors.MasterStatsActor.{MasterStats, StrategyInfo}
import pl.edu.agh.ipd.actors.PrisonerActor
import pl.edu.agh.ipd.config.{IpdConfig, IpdFileConfigLoader}
import pl.edu.agh.ipd.Launcher
import pl.edu.agh.ipd.model.Strategy.Cooperativity
import pl.edu.agh.ipd.model.map.EnergyProportionalSamplingMap.{PrisonersEnergyProvider, StrategyStats}
import pl.edu.agh.ipd.model.map.SimilarCooperativityNetwork.CooperativityProvider
import pl.edu.agh.ipd.model.{Mutations, Strategy}
import pl.edu.agh.ipd.model.map.{GraphNetwork, NetworkMap}

import scala.collection.immutable.Seq
import scala.collection.mutable
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps
import scala.util.{Random, Try}

object SeqLauncher {
  class SeqCooperativityProvider(prisoners: Seq[Prisoner]) extends CooperativityProvider {
    override def prisonersCooperativity(): IndexedSeq[Cooperativity] =
      prisoners.map(p => p.strategyView().cooperativity).toIndexedSeq
  }

  class SeqPrisonersEnergyProvider(prisoners: Seq[Prisoner]) extends PrisonersEnergyProvider {
    override def localStrategies(): IndexedSeq[StrategyStats] = {
      prisoners.map(p => StrategyStats(p.index, p.energy)).toIndexedSeq
    }
  }


  def main(args: Array[String]): Unit = {
    val uuid: UUID = UUID.randomUUID()

    val timeLimit: Int = args(0).toInt
    val config: IpdConfig = IpdFileConfigLoader.get(args(1))
    config.validate()
    val csvInterval: Int = Try(args(2).toInt).getOrElse(10)
    val csvFilename: String = Try(args(3)).getOrElse("stats.csv")

    val fights = mutable.ArrayBuffer.fill(config.populationSize)(0)
    val fightsSinceMutation = mutable.ArrayBuffer.fill(config.populationSize)(0)
    val energyChangeSinceMutation = mutable.ArrayBuffer.fill(config.populationSize)(0.0)
    val mutations = mutable.ArrayBuffer.fill(config.populationSize)(0)
    var deaths: Int = 0

    var allFights: Long = 0
    var allMutations: Long = 0

    val prisoners: Seq[Prisoner] = Vector.tabulate(config.populationSize)(new Prisoner(_, config.initialEnergy, config.initialStrategy()))
    val cooperativityProvider = new SeqCooperativityProvider(prisoners)
    val localStrategiesProvider = new SeqPrisonersEnergyProvider(prisoners)
    val networkMap: NetworkMap = config.map.newInstance().asInstanceOf[NetworkMap]
    networkMap.init(
      config.populationSize,
      config.mapOptions
        .updated("cooperativityProvider", cooperativityProvider)
        .updated("localStrategiesProvider", localStrategiesProvider)
    )
    val crossoverMap: NetworkMap = config.crossoverMap.newInstance().asInstanceOf[NetworkMap]
    crossoverMap.init(
      config.populationSize,
      config.crossoverMapOptions
        .updated("cooperativityProvider", cooperativityProvider)
        .updated("localStrategiesProvider", localStrategiesProvider)
    )

    val start = System.nanoTime()
    def fromStart(): Long = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - start)
    var lastStats = fromStart()

    while (fromStart() < timeLimit) {
      if (config.mutationMgrsCount > 0) {
        while (fights.sum.toDouble / mutations.sum > config.targetFightsToMutationsRatio) {
          prisoners.foreach { prisoner =>
            prisoner.updateStrategy(
              Mutations.mutate(prisoner.strategyView()),
              if (config.replaceEnergyOnMutation) Some(config.initialEnergy) else None
            )
            mutations(prisoner.index) += 1
            allMutations += 1
            fightsSinceMutation(prisoner.index) = 0
            energyChangeSinceMutation(prisoner.index) = 0
          }
        }
      }

      if (config.fightMgrsCount > 0) {
        prisoners.foreach { prisoner =>
          val neighbours = networkMap.findNeighbours(prisoner.index).map(prisoners)
          neighbours.filter(_.index < prisoner.index).foreach { neighbour =>
            if (config.separateStrategies) {
              prisoner.updateStrategy(prisoner.strategyView().copy(), None)
              neighbour.updateStrategy(neighbour.strategyView().copy(), None)
            }
            do {
              val moveA = prisoner.nextAction()
              fights(prisoner.index) += 1
              fightsSinceMutation(prisoner.index) += 1
              val moveB = neighbour.nextAction()
              fights(neighbour.index) += 1
              fightsSinceMutation(neighbour.index) += 1

              allFights += 1

              val (payoffA, payoffB) = PrisonerActor.Action.resolveFight(config.payoffs)(moveA, moveB)
              energyChangeSinceMutation(prisoner.index) += payoffA
              energyChangeSinceMutation(neighbour.index) += payoffB
              if (prisoner.payoff(payoffA, moveB)) die(prisoner)
              if (neighbour.payoff(payoffB, moveA)) die(neighbour)
            } while (Random.nextDouble() < config.continuationProbability)
          }

          val currentFromStart = fromStart()
          if ((currentFromStart - lastStats) / csvInterval >= 1) {
            stats(false)
            lastStats = fromStart()
          }
        }
      }

      if (config.forceEpochDeaths) {
        prisoners.foreach(die)
      }
    }
    stats(true)

    def resetPrisoner(prisoner: Prisoner): Unit = {
      val neighbors = crossoverMap.findNeighbours(prisoner.index)
      val strategy: Strategy = neighbors.length match {
        case n if n >= 2 =>
          val i = Random.nextInt(n)
          val j = {
            var tmp = i
            while (i == tmp) tmp = Random.nextInt(n)
            tmp
          }

          Strategy.crossover(prisoners(i).strategyView(), prisoners(j).strategyView())
        case 1 =>
          prisoners(neighbors.head).strategyView().copy()
        case _ =>
          prisoner.strategyView().copy()
      }
      prisoner.reset(Mutations.mutate(strategy), config.initialEnergy)
    }

    def die(prisoner: Prisoner): Unit = {
      resetPrisoner(prisoner)
      deaths += 1
      fights(prisoner.index) = 0
      fightsSinceMutation(prisoner.index) = 0
      mutations(prisoner.index) = 0
      energyChangeSinceMutation(prisoner.index) = 0
    }

    def stats(f: Boolean): Unit = {
      val energies = prisoners.map(_.energy)
      val energySum = energies.sum
      val lastMovesCoops = prisoners.flatMap(_.lastMoves).count(_ == PrisonerActor.Cooperate)
      val lastMoves = prisoners.flatMap(_.lastMoves).count(_ != null)
      val stats: MasterStats = MasterStats(new Date((start / 1e3).toLong), config.populationSize,
        energySum, energySum / config.populationSize, energies.max, energies.min,
        allFights, fights.sum / 2, fights.sum / config.populationSize, fights.max, fights.min,
        0, 0, 0, lastMovesCoops, lastMoves, deaths,
        allMutations, mutations.sum, mutations.sum / config.populationSize, mutations.max, mutations.min,
        prisoners.map(_.strategyView().cooperativity).sum / config.populationSize,
        prisoners.sortBy(_.energy).reverse
          .map(p => StrategyInfo(p.index.toString, p.energy, energyChangeSinceMutation(p.index), fightsSinceMutation(p.index), p.strategyView()))
          .take(config.topNStrategies)
      )
      println(stats.pretty)
      Launcher.writeCsvStats(csvFilename, uuid, sequential = true, config, stats, lastStats, `final` = f, 0 seconds, timeLimit seconds, timeLimit seconds)
    }
  }
}

package pl.edu.agh.ipd.utils

import java.io.{File, PrintWriter}

import scala.collection.JavaConverters._
import org.jgrapht.generate.ScaleFreeGraphGenerator
import org.jgrapht.graph.SimpleGraph
import org.jgrapht.{EdgeFactory, Graphs, VertexFactory}

import scala.io.Source

object GraphGeneration {
  class IntVertexFactory extends VertexFactory[Int] {
    private var i: Int = -1
    override def createVertex(): Int = {
      i += 1
      i
    }
  }

  class IntEdgeFactory extends EdgeFactory[Int, Int] {
    private var i: Int = -1
    override def createEdge(sourceVertex: Int, targetVertex: Int): Int = {
      i += 1
      i
    }
  }

  def main(args: Array[String]): Unit = {
    for (n <- Seq(20)) {
      val pw = new PrintWriter(new File(s"graphs/$n.txt"))

      val generator = new ScaleFreeGraphGenerator[Int, Int](n, 0)
      val graph = new SimpleGraph[Int, Int](new IntEdgeFactory)
      generator.generateGraph(graph, new IntVertexFactory, null)
      graph.vertexSet().asScala.foreach { v =>
        Graphs.neighborListOf(graph, v).asScala.filter(_ > v).foreach { u =>
          pw.write(s"$v $u\n")
        }
      }
      pw.flush()
      pw.close()
      println(graph.vertexSet().size())
    }
  }

  def read(filename: String): Map[Int, Set[Int]] = {
    Source.fromFile(s"graphs/$filename").getLines()
      .flatMap(s => {
        val ints = s.split(" ").take(2).map(_.toInt)
        Seq((ints(0), ints(1)), (ints(1), ints(0)))
      }).toSeq
      .groupBy(_._1)
      .mapValues(_.map(_._2).toSet)
  }
}

package pl.edu.agh.ipd.actors

import akka.actor.ActorSelection
import akka.pattern.ask
import akka.testkit.{TestActorRef, TestProbe}
import org.scalatest.BeforeAndAfterAll
import pl.edu.agh.ipd.actors.map.ArenaMapActor
import pl.edu.agh.ipd.actors.map.ArenaMapActor.Neighbourhood
import pl.edu.agh.ipd.model.{Strategies, Strategy}
import pl.edu.agh.ipd.utils.IpdActorTest

import scala.concurrent.duration.DurationDouble
import scala.language.postfixOps

class CrossoverMgrActorTest extends IpdActorTest with BeforeAndAfterAll {

  "CrossoverMgrActor" should {
    "prepare new strategy for a terminated actor" in {
      val prisonerProbe: TestProbe = TestProbe()
      val neighbourProbe: TestProbe = TestProbe()
      val neighbourProbe2: TestProbe = TestProbe()
      val prisoners: Seq[ActorSelection] = Seq(system.actorSelection(prisonerProbe.ref.path))
      val neighbours: Neighbourhood = IndexedSeq(system.actorSelection(neighbourProbe.ref.path), system.actorSelection(neighbourProbe2.ref.path))
      val mapProbe: TestProbe = TestProbe()
      val map: ActorSelection = system.actorSelection(mapProbe.ref.path)
      val crossoverMgrActor = TestActorRef[CrossoverMgrActor](CrossoverMgrActor.props(1, map, 10 seconds))

      var resp: Strategy = null

      crossoverMgrActor ? CrossoverMgrActor.NewStrategyRequest(prisoners(0)) onSuccess {
        case s: Strategy =>
          resp = s
      }

      mapProbe.expectMsg(ArenaMapActor.FindNeighbours(prisoners(0)))
      mapProbe.reply(neighbours)

      neighbourProbe.expectMsg(PrisonerActor.StrategyRequest)
      neighbourProbe2.expectMsg(PrisonerActor.StrategyRequest)
      neighbourProbe.reply(Strategies.getStrategy("AllCooperate"))
      neighbourProbe2.reply(Strategies.getStrategy("AllDefect"))

      eventually {
        resp shouldNot be(null)
      }
    }

    "prepare new strategy for a terminated actor with only one neighour" in {
      val prisonerProbe: TestProbe = TestProbe()
      val neighbourProbe: TestProbe = TestProbe()
      val prisoners: Seq[ActorSelection] = Seq(system.actorSelection(prisonerProbe.ref.path))
      val neighbours: Neighbourhood = IndexedSeq(system.actorSelection(neighbourProbe.ref.path))
      val mapProbe: TestProbe = TestProbe()
      val map: ActorSelection = system.actorSelection(mapProbe.ref.path)
      val crossoverMgrActor = TestActorRef[CrossoverMgrActor](CrossoverMgrActor.props(1, map, 10 seconds))

      var resp: Strategy = null

      crossoverMgrActor ? CrossoverMgrActor.NewStrategyRequest(prisoners(0)) onSuccess {
        case s: Strategy =>
          resp = s
      }

      mapProbe.expectMsg(ArenaMapActor.FindNeighbours(prisoners(0)))
      mapProbe.reply(neighbours)

      neighbourProbe.expectMsg(PrisonerActor.StrategyRequest)
      neighbourProbe.reply(Strategies.getStrategy("TitForTat"))

      eventually {
        resp shouldNot be(null)
      }
    }
  }
}

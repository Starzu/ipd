package pl.edu.agh.ipd.actors

import java.util.UUID

import akka.actor.ActorSelection
import akka.testkit.{TestActorRef, TestProbe}
import org.scalatest.BeforeAndAfterAll
import pl.edu.agh.ipd.actors.map.ArenaMapActor.Neighbourhood
import pl.edu.agh.ipd.actors.FightMgrActor._
import pl.edu.agh.ipd.actors.LocalStatsActor.FightTimeout
import pl.edu.agh.ipd.actors.MasterStatsActor.StartStats
import pl.edu.agh.ipd.actors.PrisonerActor.Energy
import pl.edu.agh.ipd.actors.map.ArenaMapActor
import pl.edu.agh.ipd.config.IpdConfig.Payoffs
import pl.edu.agh.ipd.utils.IpdActorTest

import scala.collection.mutable
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

class FightMgrActorTest extends IpdActorTest with BeforeAndAfterAll {
  val prisonerProbe: TestProbe = TestProbe()
  val neighbourProbe: TestProbe = TestProbe()
  val prisoners: Seq[ActorSelection] = Seq(system.actorSelection(prisonerProbe.ref.path))
  val neighbours: Neighbourhood = IndexedSeq(system.actorSelection(neighbourProbe.ref.path))
  val statsProbe: TestProbe = TestProbe()
  val stats: ActorSelection = system.actorSelection(statsProbe.ref.path)
  val mapProbe: TestProbe = TestProbe()
  val map: ActorSelection = system.actorSelection(mapProbe.ref.path)
  private val payoffs: Payoffs = Payoffs(2, 1, 0, -1)
  val fightId = FightId(5)
  val fightMgrActor = TestActorRef[FightMgrActor](
    FightMgrActor.props(prisoners, map, stats, payoffs, 500 milliseconds, 0.0, false, fightId))

  override def beforeAll =
    fightMgrActor ! NextFight

  "FightMgrActor" should {
    "run and resolve fight - both cooperate" in {
      runAndResolveFight(PrisonerActor.Cooperate, PrisonerActor.Cooperate, payoffs.reward, payoffs.reward)
    }

    "run and resolve fight - one cooperates, other defects" in {
      runAndResolveFight(PrisonerActor.Cooperate, PrisonerActor.Defect, payoffs.sucker, payoffs.temptation)
    }

    "run and resolve fight - one defects, other cooperates" in {
      runAndResolveFight(PrisonerActor.Defect, PrisonerActor.Cooperate, payoffs.temptation, payoffs.sucker)
    }

    "run and resolve fight - both defect" in {
      runAndResolveFight(PrisonerActor.Defect, PrisonerActor.Defect, payoffs.punishment, payoffs.punishment)
    }

    "run fight and restart after timeout" in {
      mapProbe.expectMsg(MessageWithFightId(fightId, ArenaMapActor.FindNeighbours(prisoners(0))))
      mapProbe.reply(MessageWithFightId(fightId, neighbours))

      prisonerProbe.expectMsg(MessageWithFightId(fightId, PrisonerActor.NextMoveRequest))
      neighbourProbe.expectMsg(MessageWithFightId(fightId, PrisonerActor.NextMoveRequest))
      statsProbe.expectNoMsg()

      fightMgrActor ! StartStats

      mapProbe.expectMsg(MessageWithFightId(fightId, ArenaMapActor.FindNeighbours(prisoners(0))))
      mapProbe.reply(MessageWithFightId(fightId, neighbours))

      prisonerProbe.expectMsg(MessageWithFightId(fightId, PrisonerActor.NextMoveRequest))
      neighbourProbe.expectMsg(MessageWithFightId(fightId, PrisonerActor.NextMoveRequest))
      statsProbe.expectMsg(FightTimeout(false))
    }

    "repeat fights with continuation probability (1.0)" in {
      val mapProbe: TestProbe = TestProbe()
      val map: ActorSelection = system.actorSelection(mapProbe.ref.path)
      var prisoner = TestProbe()
      val neighbour = TestProbe()
      val prisonerSelection = system.actorSelection(prisoner.ref.path)
      val neighbourSelection = system.actorSelection(neighbour.ref.path)
      val prisoners: Seq[ActorSelection] = Seq(prisonerSelection)
      val fightMgrActor = TestActorRef[FightMgrActor](
        FightMgrActor.props(prisoners, map, stats, payoffs, 10000 milliseconds, 1.0, forceEpochDeaths = false, fightId))
      fightMgrActor ! NextFight

      mapProbe.expectMsgPF() {
        case MessageWithFightId(_, ArenaMapActor.FindNeighbours(s)) if s == prisonerSelection => //ok
      }
      mapProbe.reply(MessageWithFightId(fightId, IndexedSeq(neighbourSelection)))
      mapProbe.expectNoMsg()
      for (_ <- 1 to 10) {
        prisoner.expectMsgPF() {
          case MessageWithFightId(_, PrisonerActor.NextMoveRequest) => // ok
        }
        prisoner.reply(MessageWithFightId(fightId, PrisonerActor.Cooperate))
        neighbour.expectMsgPF() {
          case MessageWithFightId(_, PrisonerActor.NextMoveRequest) => // ok
        }
        neighbour.reply(MessageWithFightId(fightId, PrisonerActor.Cooperate))
        prisoner.expectMsgPF() {
          case MessageWithFightId(_, _: PrisonerActor.FightResult) => // ok
        }
        neighbour.expectMsgPF() {
          case MessageWithFightId(_, _: PrisonerActor.FightResult) => // ok
        }
      }
    }

    "repeat fights with continuation probability (0.0)" in {
      val mapProbe: TestProbe = TestProbe()
      val map: ActorSelection = system.actorSelection(mapProbe.ref.path)
      var prisoner = TestProbe()
      val neighbour = TestProbe()
      val prisonerSelection = system.actorSelection(prisoner.ref.path)
      val neighbourSelection = system.actorSelection(neighbour.ref.path)
      val prisoners: Seq[ActorSelection] = Seq(prisonerSelection)
      val fightMgrActor = TestActorRef[FightMgrActor](
        FightMgrActor.props(prisoners, map, stats, payoffs, 100 milliseconds, 0.0, forceEpochDeaths = false, fightId))
      fightMgrActor ! NextFight

      for (_ <- 1 to 10) {
        mapProbe.expectMsgPF() {
          case MessageWithFightId(_, ArenaMapActor.FindNeighbours(s)) if s == prisonerSelection => //ok
        }
        mapProbe.reply(MessageWithFightId(fightId, IndexedSeq(neighbourSelection)))
        prisoner.expectMsgPF() {
          case MessageWithFightId(_, PrisonerActor.NextMoveRequest) => // ok
        }
        neighbour.expectMsgPF() {
          case MessageWithFightId(_, PrisonerActor.NextMoveRequest) => // ok
        }
      }
    }
  }

  def runAndResolveFight(prisonersAction: PrisonerActor.Action,
                         neighboursAction: PrisonerActor.Action,
                         prisonersPayoff: Energy,
                         neighboursPayoff: Energy) = {
    mapProbe.expectMsg(MessageWithFightId(fightId, ArenaMapActor.FindNeighbours(prisoners(0))))
    mapProbe.reply(MessageWithFightId(fightId, neighbours))

    prisonerProbe.expectMsg(MessageWithFightId(fightId, PrisonerActor.NextMoveRequest))
    neighbourProbe.expectMsg(MessageWithFightId(fightId, PrisonerActor.NextMoveRequest))
    prisonerProbe.reply(MessageWithFightId(fightId, prisonersAction))

    neighbourProbe.reply(MessageWithFightId(FightId(123), PrisonerActor.Cooperate))
    neighbourProbe.reply(MessageWithFightId(fightId, neighboursAction))
    neighbourProbe.reply(MessageWithFightId(FightId(645), PrisonerActor.Defect))

    prisonerProbe.expectMsg(MessageWithFightId(fightId, PrisonerActor.FightResult(neighboursAction, prisonersPayoff)))
    neighbourProbe.expectMsg(MessageWithFightId(fightId, PrisonerActor.FightResult(prisonersAction, neighboursPayoff)))
  }
}

package pl.edu.agh.ipd.actors

import akka.actor.{Actor, ActorSelection, Props}
import akka.testkit.{TestActorRef, TestProbe}
import pl.edu.agh.ipd.model.{Strategies, StrategyView}
import pl.edu.agh.ipd.utils.IpdActorTest

import scala.collection.mutable
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

class MutationMgrActorTest extends IpdActorTest {
  import MutationMgrActor._

  "MutationMgrActor" should {
    "ask prisoner for the strategy and send him its mutated version (repeatedly to random prisoner)" in {
      val stats = TestProbe()
      val strategy = Strategies.getStrategy("AllCooperate")
      val strategy2 = Strategies.getStrategy("AllDefect")
      val replaces = mutable.ArrayBuffer.empty[Any]
      val replaces2 = mutable.ArrayBuffer.empty[Any]
      val prisonerProbe = system.actorOf(Props(classOf[TestPrisonerActor], replaces, strategy))
      val prisonerProbe2 = system.actorOf(Props(classOf[TestPrisonerActor], replaces2, strategy2))
      val prisoners: Seq[ActorSelection] = Seq(
        system.actorSelection(prisonerProbe.path),
        system.actorSelection(prisonerProbe2.path)
      )
      val mutationMgrActor = TestActorRef[MutationMgrActor](
        MutationMgrActor.props(prisoners, system.actorSelection(stats.ref.path), 0, 1, None, 30 minutes)
      )

      mutationMgrActor ! StartMutation

      eventually {
        replaces.count(_.isInstanceOf[PrisonerActor.ReplaceStrategy]) >= 1 should be(true)
        replaces2.count(_.isInstanceOf[PrisonerActor.ReplaceStrategy]) >= 1 should be(true)
      }
    }

    "restart after timeout" in {
      val stats = TestProbe()
      val prisonerProbe: TestProbe = TestProbe()
      val prisoners: Seq[ActorSelection] = Seq(
        system.actorSelection(prisonerProbe.ref.path)
      )
      val mutationMgrActor = TestActorRef[MutationMgrActor](
        MutationMgrActor.props(prisoners, system.actorSelection(stats.ref.path), 0, 1, None, 500 millis)
      )

      mutationMgrActor ! StartMutation

      prisonerProbe.expectMsg(PrisonerActor.StrategyRequest)
      prisonerProbe.expectMsg(PrisonerActor.StrategyRequest)
    }

    "not exceed fights to mutations ratio" in {
      val stats = TestProbe()
      val prisonerProbe: TestProbe = TestProbe()
      val strategy = Strategies.getStrategy("AllCooperate")
      val prisoners: Seq[ActorSelection] = Seq(
        system.actorSelection(prisonerProbe.ref.path)
      )
      val mutationMgrActor = TestActorRef[MutationMgrActor](
        MutationMgrActor.props(prisoners, system.actorSelection(stats.ref.path), 2, 1, None, 100 millis)
      )

      stats.expectMsg(LocalStatsActor.FightsToMutationsRequest)
      stats.reply(LocalStatsActor.FightsToMutations(199, 100))

      mutationMgrActor ! StartMutation

      prisonerProbe.expectNoMsg()

      stats.expectMsg(LocalStatsActor.FightsToMutationsRequest)
      stats.reply(LocalStatsActor.FightsToMutations(1990, 100))

      prisonerProbe.expectMsg(PrisonerActor.StrategyRequest)
      prisonerProbe.reply(strategy.view())
      prisonerProbe.expectMsgPF() { case _: PrisonerActor.ReplaceStrategy => }
    }
  }
}

private class TestPrisonerActor(replaces: mutable.ArrayBuffer[Any], strategy: StrategyView) extends Actor {
  override def receive: Receive = {
    case PrisonerActor.StrategyRequest =>
      sender() ! strategy
    case msg =>
      replaces += msg
  }
}

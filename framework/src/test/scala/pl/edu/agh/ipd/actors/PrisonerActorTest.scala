package pl.edu.agh.ipd.actors

import java.util.UUID

import akka.testkit.{TestActorRef, TestProbe}
import pl.edu.agh.ipd.actors.FightMgrActor.{FightId, MessageWithFightId}
import pl.edu.agh.ipd.actors.LocalArenaActor.KillMe
import pl.edu.agh.ipd.actors.MasterStatsActor.StartStats
import pl.edu.agh.ipd.actors.PrisonerActor.{ReplaceStrategy, _}
import pl.edu.agh.ipd.model.Strategy.{State, Transition}
import pl.edu.agh.ipd.model.{Strategies, Strategy, StrategyView}
import pl.edu.agh.ipd.utils.IpdActorTest

import scala.concurrent.duration.DurationDouble
import scala.language.postfixOps
import scala.util.Random

class PrisonerActorTest extends IpdActorTest {
  val state = new State(Cooperate)
  class MockStrategy extends Strategy(Map(state -> Transition(state, state)), state) {
    var _nextMove: Action = Cooperate
    var _opponentMove: Action = Cooperate
    override def nextMove(): Action =
      _nextMove
    override def opponentMove(action: Action): Unit =
      _opponentMove = action
    override def view(): StrategyView = this
    override def copy(initState: State = this.initState, transitions: Map[State, Transition] = this.transitions): Strategy = this
  }

  "PrisonerActor" should {
    "restart if energy equals zero" in {
      val prisonerActor = TestActorRef[PrisonerActor](PrisonerActor.props(1.0, new MockStrategy, system.actorSelection("../stats"), 60 seconds, 1000, false), self)
      prisonerActor ! StartStats
      prisonerActor ! MessageWithFightId(FightId(1), FightResult(Cooperate, -1))
      expectMsg(KillMe)
    }

    "restart if energy is below zero" in {
      val prisonerActor = TestActorRef[PrisonerActor](PrisonerActor.props(1.0, new MockStrategy, system.actorSelection("../stats"), 60 seconds, 1000, false), self)
      prisonerActor ! StartStats
      prisonerActor ! MessageWithFightId(FightId(1), FightResult(Cooperate, -2))
      expectMsg(KillMe)
    }

    "give next move" in {
      val strategy: MockStrategy = new MockStrategy
      val prisonerActor = TestActorRef[PrisonerActor](PrisonerActor.props(1.0, strategy, system.actorSelection("../stats"), 60 seconds, 1000, false))
      for (_ <- 0 to 20) {
        strategy._nextMove = if (Random.nextBoolean()) Cooperate else Defect
        val id = FightId(844)
        prisonerActor ! MessageWithFightId(id, NextMoveRequest)
        expectMsg(MessageWithFightId(id, strategy._nextMove))
      }
    }

    "separate strategies" in {
      val strategy: Strategy = Strategies.titForTat
      val prisonerActor = TestActorRef[PrisonerActor](PrisonerActor.props(1.0, strategy, system.actorSelection("../stats"), 60 seconds, 1000, true))
      prisonerActor ! StartStats

      prisonerActor ! MessageWithFightId(FightId(1), NextMoveRequest)
      expectMsg(MessageWithFightId(FightId(1), PrisonerActor.Cooperate))
      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 1))

      prisonerActor ! MessageWithFightId(FightId(2), NextMoveRequest)
      expectMsg(MessageWithFightId(FightId(2), PrisonerActor.Cooperate))
      prisonerActor ! MessageWithFightId(FightId(2), FightResult(PrisonerActor.Defect, 1))

      prisonerActor ! MessageWithFightId(FightId(2), NextMoveRequest)
      expectMsg(MessageWithFightId(FightId(2), PrisonerActor.Defect))
      prisonerActor ! MessageWithFightId(FightId(2), FightResult(PrisonerActor.Cooperate, 1))

      prisonerActor ! MessageWithFightId(FightId(1), NextMoveRequest)
      expectMsg(MessageWithFightId(FightId(1), PrisonerActor.Cooperate))
      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 1))
    }

    "update strategy according to fight result" in {
      val strategy: MockStrategy = new MockStrategy
      val prisonerActor = TestActorRef[PrisonerActor](PrisonerActor.props(1.0, strategy, system.actorSelection("../stats"), 60 seconds, 1000, false))
      prisonerActor ! StartStats
      for (_ <- 0 to 20) {
        val move = if (Random.nextBoolean()) Cooperate else Defect
        prisonerActor ! MessageWithFightId(FightId(1), FightResult(move, 0.0))
        strategy._opponentMove should be(move)
      }
    }

    "provide and replace strategy" in {
      val stats = TestProbe()
      val initialStrategy = new MockStrategy
      val newStrategy = new MockStrategy
      val newStrategy2 = new MockStrategy
      val prisonerActor = TestActorRef[PrisonerActor](PrisonerActor.props(1.0, initialStrategy, system.actorSelection(stats.ref.path), 60 seconds, 1000, false))
      prisonerActor ! StartStats
      prisonerActor ! StrategyRequest
      expectMsg(initialStrategy)
      prisonerActor ! ReplaceStrategy(newStrategy, None)
      prisonerActor ! StrategyRequest
      expectMsg(newStrategy)
      prisonerActor ! ReplaceStrategy(newStrategy2, Some(300))
      prisonerActor ! SendStats
      stats.expectMsgPF() {
        case v: LocalStatsActor.PrisonerState =>
          v.energy should be(300)
        case _ => fail()
      }
    }

    "collect stats after stats start" in {
      val initialStrategy = new MockStrategy
      val newStrategy = new MockStrategy
      val stats = TestProbe()
      val prisonerActor = TestActorRef[PrisonerActor](PrisonerActor.props(1.0, initialStrategy, system.actorSelection(stats.ref.path), 60 seconds, 1000, false))

      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 1))
      prisonerActor ! SendStats
      stats.expectNoMsg()

      prisonerActor ! StartStats
      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 2))
      prisonerActor ! SendStats
      stats.expectMsg(LocalStatsActor.PrisonerState(3.0, 1, 0, 0, 0, initialStrategy, 2.0, 1))

      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Defect, -1))
      prisonerActor ! SendStats
      stats.expectMsg(LocalStatsActor.PrisonerState(2.0, 1, 0, 0, 0, initialStrategy, 1.0, 2))

      val mutatedStrategy = new MockStrategy
      prisonerActor ! ReplaceStrategy(mutatedStrategy, None)
      prisonerActor ! SendStats
      stats.expectMsg(LocalStatsActor.PrisonerState(2.0, 0, 0, 0, 1, mutatedStrategy, 0.0, 0))

      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 2))
      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Defect, -1))
      prisonerActor ! ReplaceStrategy(initialStrategy, None)
      prisonerActor ! SendStats
      stats.expectMsg(LocalStatsActor.PrisonerState(3.0, 2, 0, 0, 1, initialStrategy, 0.0, 0))

      prisonerActor ! SendStats
      stats.expectMsg(LocalStatsActor.PrisonerState(3.0, 0, 0, 0, 0, initialStrategy, 0.0, 0))
    }

    "sync with stubs" in {
      val initialStrategy = new MockStrategy
      val newStrategy = new MockStrategy
      val stub1 = TestProbe()
      val stub2 = TestProbe()
      val stats = TestProbe()
      val prisonerActor = TestActorRef[PrisonerActor](PrisonerActor.props(1.0, initialStrategy, system.actorSelection(stats.ref.path), 60 seconds, 1000, false))
      prisonerActor ! StartStats

      prisonerActor.tell(FightsBatch(Seq(FightResult(PrisonerActor.Cooperate, 2))), stub1.ref)
      prisonerActor.tell(FightsBatch(Seq(FightResult(PrisonerActor.Defect, -1))), stub2.ref)
      stub1.expectMsg(initialStrategy)
      stub2.expectMsg(initialStrategy)
      prisonerActor ! SendStats
      stats.expectMsg(LocalStatsActor.PrisonerState(1.0, 0, 0, 0, 0, initialStrategy, 0.0, 0))

      prisonerActor.tell(FightsBatch(Seq(FightResult(PrisonerActor.Defect, 1), FightResult(PrisonerActor.Cooperate, 2))), stub2.ref)
      prisonerActor ! SendStats
      stats.expectMsg(LocalStatsActor.PrisonerState(4.0, 2, 0, 0, 0, initialStrategy, 3.0, 2))

      val mutatedStrategy = new MockStrategy
      prisonerActor ! ReplaceStrategy(mutatedStrategy, None)
      prisonerActor.tell(FightsBatch(Seq(FightResult(PrisonerActor.Cooperate, 2))), stub1.ref)
      prisonerActor.tell(FightsBatch(Seq(FightResult(PrisonerActor.Defect, -1))), stub2.ref)
      stub1.expectMsg(mutatedStrategy)
      stub2.expectMsg(mutatedStrategy)
      prisonerActor ! SendStats
      stats.expectMsg(LocalStatsActor.PrisonerState(4.0,0, 0, 0, 1, mutatedStrategy, 0.0, 0))

      prisonerActor.tell(FightsBatch(Seq(FightResult(PrisonerActor.Cooperate, 2))), stub1.ref)
      prisonerActor.tell(FightsBatch(Seq(FightResult(PrisonerActor.Defect, -1))), stub2.ref)
      prisonerActor ! SendStats
      stats.expectMsg(LocalStatsActor.PrisonerState(5.0,2, 0, 0, 0, mutatedStrategy, 1.0, 2))
    }
  }

}

package pl.edu.agh.ipd.actors

import akka.testkit.{TestActorRef, TestProbe}
import pl.edu.agh.ipd.actors.FightMgrActor.{FightId, MessageWithFightId}
import pl.edu.agh.ipd.actors.PrisonerActor.{ReplaceStrategy, _}
import pl.edu.agh.ipd.model.Strategy.{State, Transition}
import pl.edu.agh.ipd.model.{Strategies, Strategy, StrategyView}
import pl.edu.agh.ipd.utils.IpdActorTest

import scala.concurrent.duration.DurationDouble
import scala.language.postfixOps
import scala.util.Random

class PrisonerStubActorTest extends IpdActorTest {
  val state = new State(Cooperate)
  class MockStrategy extends Strategy(Map(state -> Transition(state, state)), state) {
    var _nextMove: Action = Cooperate
    var _opponentMove: Action = Cooperate
    override def nextMove(): Action =
      _nextMove
    override def opponentMove(action: Action): Unit =
      _opponentMove = action
    override def view(): StrategyView = this
    override def copy(initState: State = this.initState, transitions: Map[State, Transition] = this.transitions): Strategy = this
  }

  "PrisonerStubActor" should {
    "return moves from strategy and update strategy state" in {
      val initialStrategy = new MockStrategy
      val original = TestProbe()
      val prisonerActor = TestActorRef[PrisonerActor](PrisonerStubActor.props(initialStrategy, system.actorSelection(original.ref.path), 20000, 2 seconds, 1000, false))

      original.expectMsg(FightsBatch(Seq.empty)) // initial timeout

      prisonerActor ! MessageWithFightId(FightId(1), PrisonerActor.NextMoveRequest)
      expectMsg(MessageWithFightId(FightId(1), PrisonerActor.Cooperate))
      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Defect, 2))
      initialStrategy._opponentMove should be(PrisonerActor.Defect)

      initialStrategy._nextMove = PrisonerActor.Defect
      prisonerActor ! MessageWithFightId(FightId(2), PrisonerActor.NextMoveRequest)
      expectMsg(MessageWithFightId(FightId(2), PrisonerActor.Defect))
      prisonerActor ! MessageWithFightId(FightId(2), FightResult(PrisonerActor.Cooperate, 2))
      initialStrategy._opponentMove should be(PrisonerActor.Cooperate)
    }

    "collect fights and sync on threshold with original actor" in {
      val initialStrategy = new MockStrategy
      val original = TestProbe()
      val prisonerActor = TestActorRef[PrisonerActor](PrisonerStubActor.props(initialStrategy, system.actorSelection(original.ref.path), 2, 2 seconds, 1000, false))

      original.expectMsg(FightsBatch(Seq.empty)) // initial timeout

      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 5))
      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Defect, -2))
      original.expectMsg(FightsBatch(Seq(FightResult(PrisonerActor.Cooperate, 5), FightResult(PrisonerActor.Defect, -2))))

      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 5))
      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 3))
      original.expectMsg(FightsBatch(Seq(FightResult(PrisonerActor.Cooperate, 5), FightResult(PrisonerActor.Cooperate, 3))))
    }

    "separate strategies" in {
      val strategy: Strategy = Strategies.titForTat
      val initialStrategy = new MockStrategy
      val original = TestProbe()
      val prisonerActor = TestActorRef[PrisonerActor](PrisonerStubActor.props(strategy, system.actorSelection(original.ref.path), 2, 2 seconds, 1000, true))

      prisonerActor ! MessageWithFightId(FightId(1), NextMoveRequest)
      expectMsg(MessageWithFightId(FightId(1), PrisonerActor.Cooperate))
      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 1))

      prisonerActor ! MessageWithFightId(FightId(2), NextMoveRequest)
      expectMsg(MessageWithFightId(FightId(2), PrisonerActor.Cooperate))
      prisonerActor ! MessageWithFightId(FightId(2), FightResult(PrisonerActor.Defect, 1))

      prisonerActor ! MessageWithFightId(FightId(2), NextMoveRequest)
      expectMsg(MessageWithFightId(FightId(2), PrisonerActor.Defect))
      prisonerActor ! MessageWithFightId(FightId(2), FightResult(PrisonerActor.Cooperate, 1))

      prisonerActor ! MessageWithFightId(FightId(1), NextMoveRequest)
      expectMsg(MessageWithFightId(FightId(1), PrisonerActor.Cooperate))
      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 1))
    }

    "collect fights and sync on timeout with original actor" in {
      val initialStrategy = new MockStrategy
      val original = TestProbe()
      val prisonerActor = TestActorRef[PrisonerActor](PrisonerStubActor.props(initialStrategy, system.actorSelection(original.ref.path), 2000, 1 seconds, 1000, false))

      original.expectMsg(FightsBatch(Seq.empty))

      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 5))
      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Defect, -2))
      original.expectMsg(FightsBatch(Seq(FightResult(PrisonerActor.Cooperate, 5), FightResult(PrisonerActor.Defect, -2))))

      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 5))
      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 3))
      original.expectMsg(FightsBatch(Seq(FightResult(PrisonerActor.Cooperate, 5), FightResult(PrisonerActor.Cooperate, 3))))
    }

    "ignore moves from the old strategy" in {
      val initialStrategy = new MockStrategy
      val updatedStrategy = new MockStrategy
      val original = TestProbe()
      val prisonerActor = TestActorRef[PrisonerActor](PrisonerStubActor.props(initialStrategy, system.actorSelection(original.ref.path), 2, 2 seconds, 1000, false))

      original.expectMsg(FightsBatch(Seq.empty)) // initial timeout

      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 7))
      prisonerActor ! updatedStrategy.view()

      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 5))
      prisonerActor ! MessageWithFightId(FightId(1), FightResult(PrisonerActor.Cooperate, 3))
      original.expectMsg(FightsBatch(Seq(FightResult(PrisonerActor.Cooperate, 5), FightResult(PrisonerActor.Cooperate, 3))))
    }

    "provide and replace strategy" in {
      val initialStrategy = new MockStrategy
      val newStrategy = new MockStrategy
      val original = TestProbe()
      val prisonerActor = TestActorRef[PrisonerActor](PrisonerStubActor.props(initialStrategy, system.actorSelection(original.ref.path), 2, 2 seconds, 1000, false))
      prisonerActor ! StrategyRequest
      expectMsg(initialStrategy)
    }
  }

}

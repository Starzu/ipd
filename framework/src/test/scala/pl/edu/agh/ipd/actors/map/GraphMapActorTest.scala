package pl.edu.agh.ipd.actors.map

import akka.actor.ActorSelection
import com.github.ghik.silencer.silent
import pl.edu.agh.ipd.actors.map.ArenaMapActor.{FindNeighbours, Neighbourhood}
import pl.edu.agh.ipd.utils.IpdActorTest

class GraphMapActorTest extends IpdActorTest {
  private val prisoners: Seq[ActorSelection] = Seq.tabulate[ActorSelection](10)(i => system.actorSelection(s"$i"))
  val mapActor = system.actorOf(ArenaMapActor.props(classOf[GraphMapActor], prisoners, Map("filename" -> "10.txt")))

  "GraphMapActor" should {
    "handle prisoner from graph" in {
      mapActor ! FindNeighbours(prisoners(0))
      expectMsgPF() {
        case neighbours: Neighbourhood@silent =>
          neighbours.toSet should be(Set(prisoners(5)))
      }

      mapActor ! FindNeighbours(prisoners(1))
      expectMsgPF() {
        case neighbours: Neighbourhood@silent =>
          neighbours.toSet should be(Set(prisoners(2), prisoners(7)))
      }

      mapActor ! FindNeighbours(prisoners(5))
      expectMsgPF() {
        case neighbours: Neighbourhood@silent =>
          neighbours.toSet should be(Set(prisoners(2), prisoners(6), prisoners(8), prisoners(9), prisoners(0)))
      }
    }
  }
}

package pl.edu.agh.ipd.actors.map

import akka.actor.ActorSelection
import com.github.ghik.silencer.silent
import pl.edu.agh.ipd.actors.map.ArenaMapActor.{FindNeighbours, Neighbourhood}
import pl.edu.agh.ipd.utils.IpdActorTest

class OneDimArenaMapActorTest extends IpdActorTest {
  private val prisoners: Seq[ActorSelection] = Seq.tabulate[ActorSelection](300)(i => system.actorSelection(s"$i"))
  val mapActor = system.actorOf(ArenaMapActor.props(classOf[OneDimArenaMapActor], prisoners, Map.empty))

  "OneDimArenaMapActor" should {
    "handle left border of prisoners list" in {
      mapActor ! FindNeighbours(prisoners(0))
      expectMsgPF() {
        case neighbours: Neighbourhood @silent =>
          neighbours.toSet should be(Set(prisoners(1)))
      }
    }

    "handle right border of prisoners list" in {
      mapActor ! FindNeighbours(prisoners(prisoners.length - 1))
      expectMsgPF() {
        case neighbours: Neighbourhood @silent =>
          neighbours.toSet should be(Set(prisoners(prisoners.length - 2)))
      }
    }

    "handle middle indices of prisoners list" in {
      mapActor ! FindNeighbours(prisoners(prisoners.length / 2))
      expectMsgPF() {
        case neighbours: Neighbourhood @silent =>
          neighbours.toSet should be(Set(prisoners(prisoners.length / 2 - 1), prisoners(prisoners.length / 2 + 1)))
      }
    }
  }
}

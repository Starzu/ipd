package pl.edu.agh.ipd.actors.map

import akka.actor.ActorSelection
import com.github.ghik.silencer.silent
import pl.edu.agh.ipd.actors.map.ArenaMapActor.{FindNeighbours, Neighbourhood}
import pl.edu.agh.ipd.utils.IpdActorTest

class TwoDimArenaMapActorTest extends IpdActorTest {

  "TwoDimArenaMapActor" when {
    "it's a full rectangle" should {
      val populationSize: Int = 300
      val mapWidth: Int = 50
      val prisoners: Seq[ActorSelection] = Seq.tabulate[ActorSelection](populationSize)(i => system.actorSelection(s"$i"))
      val mapActor = system.actorOf(ArenaMapActor.props(classOf[TwoDimArenaMapActor], prisoners, Map("width" -> mapWidth)))

      "handle left-top corner of prisoners list" in {
        val i: Int = 0
        mapActor ! FindNeighbours(prisoners(i))
        expectMsgPF() {
          case neighbours: Neighbourhood @silent =>
            neighbours.toSet should be(Set(prisoners(i + 1), prisoners(i + mapWidth)))
        }
      }

      "handle right-top corner of prisoners list" in {
        val i: Int = mapWidth - 1
        mapActor ! FindNeighbours(prisoners(i))
        expectMsgPF() {
          case neighbours: Neighbourhood @silent =>
            neighbours.toSet should be(Set(prisoners(i - 1), prisoners(i + mapWidth)))
        }
      }

      "handle left-bottom corner of prisoners list" in {
        val i: Int = populationSize - mapWidth
        mapActor ! FindNeighbours(prisoners(i))
        expectMsgPF() {
          case neighbours: Neighbourhood @silent =>
            neighbours.toSet should be(Set(prisoners(i - mapWidth), prisoners(i + 1)))
        }
      }

      "handle right-bottom corner of prisoners list" in {
        val i: Int = populationSize - 1
        mapActor ! FindNeighbours(prisoners(i))
        expectMsgPF() {
          case neighbours: Neighbourhood @silent =>
            neighbours.toSet should be(Set(prisoners(i - mapWidth), prisoners(i - 1)))
        }
      }

      "handle top border of prisoners list" in {
        val i: Int = mapWidth / 2
        mapActor ! FindNeighbours(prisoners(i))
        expectMsgPF() {
          case neighbours: Neighbourhood @silent =>
            neighbours.toSet should be(Set(prisoners(i - 1), prisoners(i + 1), prisoners(i + mapWidth)))
        }
      }

      "handle right border of prisoners list" in {
        val i: Int = 2 * mapWidth - 1
        mapActor ! FindNeighbours(prisoners(i))
        expectMsgPF() {
          case neighbours: Neighbourhood @silent =>
            neighbours.toSet should be(Set(prisoners(i - mapWidth), prisoners(i - 1), prisoners(i + mapWidth)))
        }
      }

      "handle bottom border of prisoners list" in {
        val i: Int = populationSize - mapWidth / 2
        mapActor ! FindNeighbours(prisoners(i))
        expectMsgPF() {
          case neighbours: Neighbourhood @silent =>
            neighbours.toSet should be(Set(prisoners(i - mapWidth), prisoners(i - 1), prisoners(i + 1)))
        }
      }

      "handle left border of prisoners list" in {
        val i: Int = 2 * mapWidth
        mapActor ! FindNeighbours(prisoners(i))
        expectMsgPF() {
          case neighbours: Neighbourhood @silent =>
            neighbours.toSet should be(Set(prisoners(i - mapWidth), prisoners(i + 1), prisoners(i + mapWidth)))
        }
      }

      "handle middle of prisoners list" in {
        val i: Int = (1.5 * mapWidth).toInt
        mapActor ! FindNeighbours(prisoners(i))
        expectMsgPF() {
          case neighbours: Neighbourhood @silent =>
            neighbours.toSet should be(Set(prisoners(i - mapWidth), prisoners(i - 1), prisoners(i + 1), prisoners(i + mapWidth)))
        }
      }
    }

    "it's not a full rectangle" should {
      val populationSize: Int = 300
      val mapWidth: Int = 45
      val prisoners: Seq[ActorSelection] = Seq.tabulate[ActorSelection](populationSize)(i => system.actorSelection(s"$i"))
      val mapActor = system.actorOf(ArenaMapActor.props(classOf[TwoDimArenaMapActor], prisoners, Map("width" -> mapWidth)))

      "handle last index" in {
        val i: Int = populationSize - 1
        mapActor ! FindNeighbours(prisoners(i))
        expectMsgPF() {
          case neighbours: Neighbourhood @silent =>
            neighbours.toSet should be(Set(prisoners(i - mapWidth), prisoners(i - 1)))
        }
      }

      "penultimate row" in {
        val i: Int = populationSize - mapWidth + 1
        mapActor ! FindNeighbours(prisoners(i))
        expectMsgPF() {
          case neighbours: Neighbourhood @silent =>
            neighbours.toSet should be(Set(prisoners(i - mapWidth), prisoners(i - 1), prisoners(i + 1)))
        }
      }

      "penultimate row last element" in {
        val i: Int = populationSize - populationSize % mapWidth - 1
        mapActor ! FindNeighbours(prisoners(i))
        expectMsgPF() {
          case neighbours: Neighbourhood @silent =>
            neighbours.toSet should be(Set(prisoners(i - mapWidth), prisoners(i - 1)))
        }
      }
    }
  }
}

package pl.edu.agh.ipd.actors.splitter

import pl.edu.agh.ipd.utils.IpdTest

class GraphMapSplitterTest extends IpdTest {
  val splitter = new GraphMapSplitter
  splitter.init(Map("filename" -> "10.txt"))

  "GraphMapSplitter" should {
    "not split prisoners for one host" in {
      val hostsPrisoners = splitter.splittingStrategy(10, 1, useStubs = false)
      hostsPrisoners.local.keys should contain theSameElementsAs Seq(0)
      hostsPrisoners.local(0) should contain theSameElementsAs(0 to 9)
      hostsPrisoners.stubs(0).isEmpty should be(true)

      val hostsPrisoners2 = splitter.splittingStrategy(10, 1, useStubs = true)
      hostsPrisoners2.local.keys should contain theSameElementsAs Seq(0)
      hostsPrisoners2.local(0) should contain theSameElementsAs(0 to 9)
      hostsPrisoners2.stubs(0).isEmpty should be(true)
    }

    "split prisoners for 2 hosts and not create stubs" in {
      val hostsPrisoners = splitter.splittingStrategy(10, 2, useStubs = false)
      hostsPrisoners.local.keys should contain theSameElementsAs Seq(0, 1)
      hostsPrisoners.local(0) should contain theSameElementsAs Seq(5, 6, 8, 9, 0)
      hostsPrisoners.local(1) should contain theSameElementsAs Seq(2, 1, 3, 4, 7)
      hostsPrisoners.stubs.values.forall(_.isEmpty) should be(true)
    }

    "split prisoners for 2 hosts and create stubs" in {
      val hostsPrisoners = splitter.splittingStrategy(10, 2, useStubs = true)
      hostsPrisoners.local.keys should contain theSameElementsAs Seq(0, 1)
      hostsPrisoners.local(0) should contain theSameElementsAs Seq(5, 6, 8, 9, 0)
      hostsPrisoners.local(1) should contain theSameElementsAs Seq(2, 1, 3, 4, 7)
      hostsPrisoners.stubs(0) should contain theSameElementsAs Seq(2)
      hostsPrisoners.stubs(1) should contain theSameElementsAs Seq(5, 8)
    }

    "split prisoners for 3 hosts and not create stubs" in {
      val hostsPrisoners = splitter.splittingStrategy(10, 3, useStubs = false)
      hostsPrisoners.local.keys should contain theSameElementsAs Seq(0, 1, 2)
      hostsPrisoners.local(0) should contain theSameElementsAs Seq(1, 7)
      hostsPrisoners.local(1) should contain theSameElementsAs Seq(5, 6, 8, 9, 0)
      hostsPrisoners.local(2) should contain theSameElementsAs Seq(2, 3, 4)
      hostsPrisoners.stubs.values.forall(_.isEmpty) should be(true)
    }

    "split prisoners for 3 hosts and create stubs" in {
      val hostsPrisoners = splitter.splittingStrategy(10, 3, useStubs = true)
      hostsPrisoners.local.keys should contain theSameElementsAs Seq(0, 1, 2)
      hostsPrisoners.local(0) should contain theSameElementsAs Seq(1, 7)
      hostsPrisoners.local(1) should contain theSameElementsAs Seq(5, 6, 8, 9, 0)
      hostsPrisoners.local(2) should contain theSameElementsAs Seq(2, 3, 4)
      hostsPrisoners.stubs(0) should contain theSameElementsAs Seq(2)
      hostsPrisoners.stubs(1) should contain theSameElementsAs Seq(2)
      hostsPrisoners.stubs(2) should contain theSameElementsAs Seq(1, 5, 7, 8)
    }
  }
}

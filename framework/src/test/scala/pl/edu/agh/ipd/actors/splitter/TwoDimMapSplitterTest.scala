package pl.edu.agh.ipd.actors.splitter

import pl.edu.agh.ipd.utils.IpdTest

class TwoDimMapSplitterTest extends IpdTest {
  val splitter = new TwoDimArenaMapSplitter

  "TwoDimMapSplitter" should {
    "not split prisoners for one host" in {
      splitter.init(Map("width" -> 3))
      val hostsPrisoners = splitter.splittingStrategy(16, 1, useStubs = false)
      hostsPrisoners.local.keys should contain theSameElementsAs Seq(0)
      hostsPrisoners.local(0) should contain theSameElementsAs(0 to 15)
      hostsPrisoners.stubs(0).isEmpty should be(true)

      splitter.init(Map("width" -> 6))
      val hostsPrisoners2 = splitter.splittingStrategy(16, 1, useStubs = true)
      hostsPrisoners2.local.keys should contain theSameElementsAs Seq(0)
      hostsPrisoners2.local(0) should contain theSameElementsAs(0 to 15)
      hostsPrisoners2.stubs(0).isEmpty should be(true)
    }

    "split prisoners for 2 hosts and not create stubs" in {
      splitter.init(Map("width" -> 3))
      val hostsPrisoners = splitter.splittingStrategy(16, 2, useStubs = false)
      hostsPrisoners.local.keys should contain theSameElementsAs Seq(0, 1)
      hostsPrisoners.local(0) should contain theSameElementsAs(0 to 8)
      hostsPrisoners.local(1) should contain theSameElementsAs(9 to 15)
      hostsPrisoners.stubs.values.forall(_.isEmpty) should be(true)

      splitter.init(Map("width" -> 6))
      val hostsPrisoners2 = splitter.splittingStrategy(16, 2, useStubs = false)
      hostsPrisoners2.local.keys should contain theSameElementsAs Seq(0, 1)
      hostsPrisoners2.local(0) should contain theSameElementsAs Seq(0, 6, 12, 1, 7, 13, 2, 8, 14)
      hostsPrisoners2.local(1) should contain theSameElementsAs Seq(3, 9, 15, 4, 10, 5, 11)
      hostsPrisoners2.stubs.values.forall(_.isEmpty) should be(true)
    }

    "split prisoners for 2 hosts and create stubs" in {
      splitter.init(Map("width" -> 3))
      val hostsPrisoners = splitter.splittingStrategy(16, 2, useStubs = true)
      hostsPrisoners.local.keys should contain theSameElementsAs Seq(0, 1)
      hostsPrisoners.local(0) should contain theSameElementsAs(0 to 8)
      hostsPrisoners.local(1) should contain theSameElementsAs(9 to 15)
      hostsPrisoners.stubs(0) should contain theSameElementsAs(9 to 11)
      hostsPrisoners.stubs(1) should contain theSameElementsAs(6 to 8)

      splitter.init(Map("width" -> 6))
      val hostsPrisoners2 = splitter.splittingStrategy(16, 2, useStubs = true)
      hostsPrisoners2.local.keys should contain theSameElementsAs Seq(0, 1)
      hostsPrisoners2.local(0) should contain theSameElementsAs Seq(0, 6, 12, 1, 7, 13, 2, 8, 14)
      hostsPrisoners2.local(1) should contain theSameElementsAs Seq(3, 9, 15, 4, 10, 5, 11)
      hostsPrisoners2.stubs(0) should contain theSameElementsAs Seq(3, 9, 15)
      hostsPrisoners2.stubs(1) should contain theSameElementsAs Seq(2, 8, 14)
    }

    "split prisoners for 3 hosts and not create stubs" in {
      splitter.init(Map("width" -> 3))
      val hostsPrisoners = splitter.splittingStrategy(16, 3, useStubs = false)
      hostsPrisoners.local.keys should contain theSameElementsAs Seq(0, 1, 2)
      hostsPrisoners.local(0) should contain theSameElementsAs(0 to 5)
      hostsPrisoners.local(1) should contain theSameElementsAs(6 to 11)
      hostsPrisoners.local(2) should contain theSameElementsAs(12 to 15)
      hostsPrisoners.stubs.values.forall(_.isEmpty) should be(true)

      splitter.init(Map("width" -> 6))
      val hostsPrisoners2 = splitter.splittingStrategy(16, 3, useStubs = false)
      hostsPrisoners2.local.keys should contain theSameElementsAs Seq(0, 1, 2)
      hostsPrisoners2.local(0) should contain theSameElementsAs Seq(0, 6, 12, 1, 7, 13)
      hostsPrisoners2.local(1) should contain theSameElementsAs Seq(2, 8, 14, 3, 9, 15)
      hostsPrisoners2.local(2) should contain theSameElementsAs Seq(4, 10, 5, 11)
      hostsPrisoners2.stubs.values.forall(_.isEmpty) should be(true)
    }

    "split prisoners for 3 hosts and create stubs" in {
      splitter.init(Map("width" -> 3))
      val hostsPrisoners = splitter.splittingStrategy(16, 3, useStubs = true)
      hostsPrisoners.local.keys should contain theSameElementsAs Seq(0, 1, 2)
      hostsPrisoners.local(0) should contain theSameElementsAs(0 to 5)
      hostsPrisoners.local(1) should contain theSameElementsAs(6 to 11)
      hostsPrisoners.local(2) should contain theSameElementsAs(12 to 15)
      hostsPrisoners.stubs(0) should contain theSameElementsAs(6 to 8)
      hostsPrisoners.stubs(1) should contain theSameElementsAs((3 to 5) ++ (12 to 14))
      hostsPrisoners.stubs(2) should contain theSameElementsAs(9 to 11)

      splitter.init(Map("width" -> 6))
      val hostsPrisoners2 = splitter.splittingStrategy(16, 3, useStubs = true)
      hostsPrisoners2.local.keys should contain theSameElementsAs Seq(0, 1, 2)
      hostsPrisoners2.local(0) should contain theSameElementsAs Seq(0, 6, 12, 1, 7, 13)
      hostsPrisoners2.local(1) should contain theSameElementsAs Seq(2, 8, 14, 3, 9, 15)
      hostsPrisoners2.local(2) should contain theSameElementsAs Seq(4, 10, 5, 11)
      hostsPrisoners2.stubs(0) should contain theSameElementsAs Seq(2, 8, 14)
      hostsPrisoners2.stubs(1) should contain theSameElementsAs Seq(1, 7, 13, 4, 10)
      hostsPrisoners2.stubs(2) should contain theSameElementsAs Seq(3, 9)
    }
  }
}

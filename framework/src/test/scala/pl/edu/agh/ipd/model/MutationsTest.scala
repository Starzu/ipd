package pl.edu.agh.ipd.model

import pl.edu.agh.ipd.actors.PrisonerActor
import pl.edu.agh.ipd.actors.PrisonerActor.{Cooperate, Defect}
import pl.edu.agh.ipd.model.Strategy.{State, Transition}
import pl.edu.agh.ipd.utils.IpdTest

import scala.concurrent.duration.DurationInt
import scala.language.postfixOps
import scala.util.Random

class MutationsTest extends IpdTest {
  implicit val patience: PatienceConfig = PatienceConfig(3 seconds, 0 milliseconds)
  val stateA = new State(Cooperate)
  val stateB = new State(Defect)
  val strategy = new Strategy(Map(
    stateA -> Transition(stateB, stateA),
    stateB -> Transition(stateA, stateB)
  ), stateA)

  val singleStateStrategy = new Strategy(Map(
    stateA -> Transition(stateA, stateA)
  ), stateA)

  "Mutations" should {
    "add state to Strategy" in {
      eventually({
        val copy = strategy.copy()
        val mutated = Mutations.addState(copy)

        eventually {
          val opponent = if (Random.nextBoolean()) Cooperate else Defect
          copy.opponentMove(opponent)
          mutated.opponentMove(opponent)

          mutated.nextMove() shouldNot be(copy.nextMove())
        }
      })
    }

    "remove state from Strategy" in {
      eventually({
        val copy = strategy.copy()
        val mutated = Mutations.removeRandomState(copy)

        eventually {
          val opponent = if (Random.nextBoolean()) Cooperate else Defect
          copy.opponentMove(opponent)
          mutated.opponentMove(opponent)

          mutated.nextMove() shouldNot be(copy.nextMove())
        }
      })
    }

    "remove init state from Strategy" in {
      val copy = strategy.copy()
      val mutated = Mutations.removeState(copy, copy.initState)
      mutated.transitions.size should be(1)
      mutated.transitions(stateB) should be(Transition(stateB, stateB))
    }

    "change action in random state of strategy" in {
      eventually({
        val copy = strategy.copy()
        val mutated = Mutations.changeActionInRandomState(copy)

        eventually {
          val opponent = if (Random.nextBoolean()) Cooperate else Defect
          copy.opponentMove(opponent)
          mutated.opponentMove(opponent)

          mutated.nextMove() shouldNot be(copy.nextMove())
        }
      })
    }

    "change action in initial state of strategy" in {
      val copy = strategy.copy()
      val mutated = Mutations.changeActionInState(copy, copy.initState)
      copy.initState.move shouldNot be(mutated.initState.move)
      def testNextState(action: PrisonerActor.Action): Unit = {
        if (copy.transitions(copy.initState).nextState(action) == copy.initState)
          mutated.transitions(mutated.initState).nextState(action) should be(mutated.initState)
        else
          copy.transitions(copy.initState).nextState(action) should be(mutated.transitions(mutated.initState).nextState(action))
      }
      testNextState(Cooperate)
      testNextState(Defect)
    }

    "change action in not initial state of strategy" in {
      val copy = strategy.copy()
      val changedState: State = copy.transitions.keys.find(_ != copy.initState).get
      val mutated = Mutations.changeActionInState(copy, changedState)
      copy.initState.move should be(mutated.initState.move)

      eventually {
        val opponent = if (Random.nextBoolean()) Cooperate else Defect
        copy.opponentMove(opponent)
        mutated.opponentMove(opponent)

        mutated.nextMove() shouldNot be(copy.nextMove())
      }
    }

    "change transition in random state of strategy" in {
      eventually({
        val copy = strategy.copy()
        val mutated = Mutations.changeTransitionFromRandomState(copy)

        eventually {
          val opponent = if (Random.nextBoolean()) Cooperate else Defect
          copy.opponentMove(opponent)
          mutated.opponentMove(opponent)

          mutated.nextMove() shouldNot be(copy.nextMove())
        }
      })
    }

    "change transition in init state of strategy" in {
      val copy = strategy.copy()
      val mutated = Mutations.changeTransitionFromState(copy, copy.initState)

      eventually {
        val opponent = if (Random.nextBoolean()) Cooperate else Defect
        copy.opponentMove(opponent)
        mutated.opponentMove(opponent)

        mutated.nextMove() shouldNot be(copy.nextMove())
      }
    }

    "handle strategy with one state" in {
      Mutations.addState(singleStateStrategy) shouldNot be(singleStateStrategy)
      Mutations.removeRandomState(singleStateStrategy) shouldNot be(singleStateStrategy)
      Mutations.removeRandomState(singleStateStrategy).transitions.size should be(1)
      Mutations.changeActionInRandomState(singleStateStrategy) shouldNot be(singleStateStrategy)
      Mutations.changeTransitionFromRandomState(singleStateStrategy) shouldNot be(singleStateStrategy)
    }
  }
}

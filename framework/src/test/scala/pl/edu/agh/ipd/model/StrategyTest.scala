package pl.edu.agh.ipd.model

import pl.edu.agh.ipd.actors.PrisonerActor.{Cooperate, Defect}
import pl.edu.agh.ipd.model.Strategy.{State, Transition}
import pl.edu.agh.ipd.utils.IpdTest

class StrategyTest extends IpdTest {
  val stateA = new State(Cooperate)
  val stateB = new State(Defect)
  val strategy = new Strategy(Map(
    stateA -> Transition(stateB, stateA),
    stateB -> Transition(stateA, stateB)
  ), stateA)

  "Strategy" should {
    "return next move" in {
      strategy.nextMove() should be(Cooperate)
      strategy.nextMove() should be(Cooperate)
    }

    "change state after opponent move" in {
      strategy.nextMove() should be(Cooperate)
      strategy.opponentMove(Cooperate)
      strategy.nextMove() should be(Defect)
      strategy.opponentMove(Defect)
      strategy.nextMove() should be(Defect)
      strategy.opponentMove(Cooperate)
      strategy.nextMove() should be(Cooperate)
      strategy.opponentMove(Defect)
      strategy.nextMove() should be(Cooperate)
    }

    "create copy of itself" in {
      val copy = strategy.copy()
      copy.nextMove() should be(Cooperate)
      copy.opponentMove(Cooperate)
      copy.nextMove() should be(Defect)
      copy.opponentMove(Defect)
      copy.nextMove() should be(Defect)
      copy.opponentMove(Cooperate)
      copy.nextMove() should be(Cooperate)
      copy.opponentMove(Defect)
      copy.nextMove() should be(Cooperate)
    }

    "create modified copy of itself" in {
      val copy = strategy.copy(initState = stateB)
      copy.nextMove() should be(Defect)
      copy.opponentMove(Cooperate)
      copy.nextMove() should be(Cooperate)
      copy.opponentMove(Defect)
      copy.nextMove() should be(Cooperate)
      copy.opponentMove(Cooperate)
      copy.nextMove() should be(Defect)
      copy.opponentMove(Defect)
      copy.nextMove() should be(Defect)

      val copy2 = strategy.view().copy(transitions = Map(
        stateA -> Transition(stateB, stateB),
        stateB -> Transition(stateA, stateB)
      ))
      copy2.nextMove() should be(Cooperate)
      copy2.opponentMove(Cooperate)
      copy2.nextMove() should be(Defect)
      copy2.opponentMove(Cooperate)
      copy2.nextMove() should be(Cooperate)
      copy2.opponentMove(Defect)
      copy2.nextMove() should be(Defect)
      copy2.opponentMove(Defect)
      copy2.nextMove() should be(Defect)
      copy2.opponentMove(Cooperate)
      copy2.nextMove() should be(Cooperate)
    }

    "remove unreachable states from transitions map" in {
      val stateC = new State(Cooperate)
      val stateD = new State(Defect)
      val stateE = new State(Defect)
      val stateF = new State(Cooperate)
      val transitions: Map[State, Transition] = Map(
        stateA -> Transition(stateB, stateA),
        stateB -> Transition(stateA, stateA),
        stateC -> Transition(stateA, stateA),
        stateD -> Transition(stateC, stateD),
        stateF -> Transition(stateF, stateF)
      )

      val strategy = new Strategy(transitions, stateA)
      strategy.transitions.keys.toSet should be(Set(stateA, stateB))

      val strategy2 = new Strategy(transitions, stateB)
      strategy2.transitions.keys.toSet should be(Set(stateA, stateB))

      val strategy3 = new Strategy(transitions, stateC)
      strategy3.transitions.keys.toSet should be(Set(stateA, stateB, stateC))

      val strategy4 = new Strategy(transitions, stateD)
      strategy4.transitions.keys.toSet should be(Set(stateA, stateB, stateC, stateD))

      intercept[IllegalArgumentException] {
        val strategy5 = new Strategy(transitions, stateE)
      }

      val strategy6 = new Strategy(transitions, stateF)
      strategy6.transitions.keys.toSet should be(Set(stateF))
    }

    "count cooperativity" in {
      Strategies.allDefect.cooperativity should be(0.0)
      Strategies.titForTat.cooperativity should be(1.0)
      Strategies.allCooperate.cooperativity should be(1.0)
      strategy.cooperativity should be(0.05)
      for (_ <- 0 until 100) {
        val randomStrategy = Strategies.random
        val randomCooperativity = randomStrategy.cooperativity
        val condition = randomCooperativity >= 0 && randomCooperativity <= 1
        if (!condition) println(randomStrategy)
        condition should be(true)
      }
    }
  }
}

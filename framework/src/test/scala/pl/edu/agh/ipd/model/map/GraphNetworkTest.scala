package pl.edu.agh.ipd.model.map

import pl.edu.agh.ipd.utils.IpdTest

class GraphNetworkTest extends IpdTest {
  val networkMap = new GraphNetwork
  networkMap.init(10, Map("filename" -> "10.txt"))

  "GraphNetworkTest" should {
    "findNeighbours" in {
      networkMap.findNeighbours(0) should contain theSameElementsAs Seq(5)
      networkMap.findNeighbours(2) should contain theSameElementsAs Seq(1, 3, 4, 5, 7, 8)
    }
  }
}

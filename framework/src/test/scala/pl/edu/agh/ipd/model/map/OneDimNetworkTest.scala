package pl.edu.agh.ipd.model.map

import pl.edu.agh.ipd.utils.IpdTest

class OneDimNetworkTest extends IpdTest {
  val networkMap = new OneDimNetwork
  networkMap.init(10, Map.empty)

  "OneDimNetworkTest" should {
    "findNeighbours" in {
      networkMap.findNeighbours(0) should be(Seq(1))
      networkMap.findNeighbours(5) should contain theSameElementsAs Seq(4, 6)
      networkMap.findNeighbours(9) should be(Seq(8))
    }
  }
}

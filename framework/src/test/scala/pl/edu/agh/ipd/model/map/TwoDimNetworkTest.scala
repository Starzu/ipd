package pl.edu.agh.ipd.model.map

import pl.edu.agh.ipd.utils.IpdTest

class TwoDimNetworkTest extends IpdTest {
  val networkMap = new TwoDimNetwork
  networkMap.init(100, Map("width" -> 5))

  "TwoDimNetworkTest" should {
    "findNeighbours" in {
      networkMap.findNeighbours(0) should contain theSameElementsAs Seq(1, 5)
      networkMap.findNeighbours(2) should contain theSameElementsAs Seq(1, 3, 7)
      networkMap.findNeighbours(4) should contain theSameElementsAs Seq(3, 9)
      networkMap.findNeighbours(10) should contain theSameElementsAs Seq(5, 11, 15)
      networkMap.findNeighbours(12) should contain theSameElementsAs Seq(11, 7, 17, 13)
      networkMap.findNeighbours(14) should contain theSameElementsAs Seq(13, 9, 19)
      networkMap.findNeighbours(95) should contain theSameElementsAs Seq(90, 96)
      networkMap.findNeighbours(97) should contain theSameElementsAs Seq(96, 92, 98)
      networkMap.findNeighbours(99) should contain theSameElementsAs Seq(94, 98)
    }
  }
}

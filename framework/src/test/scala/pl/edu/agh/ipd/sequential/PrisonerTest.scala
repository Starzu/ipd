package pl.edu.agh.ipd.sequential

import pl.edu.agh.ipd.actors.PrisonerActor
import pl.edu.agh.ipd.actors.PrisonerActor.{Cooperate, Defect}
import pl.edu.agh.ipd.model.Strategies
import pl.edu.agh.ipd.utils.IpdTest

class PrisonerTest extends IpdTest {
  "PrisonerTest" should {
    "play according to strategy" in {
      val prisoner = new Prisoner(0, 10, Strategies.titForTat)

      prisoner.nextAction() should be(Cooperate)
      prisoner.payoff(3, Cooperate) should be(false)
      prisoner.nextAction() should be(Cooperate)
      prisoner.payoff(-1, Defect) should be(false)
      prisoner.nextAction() should be(Defect)
    }

    "updateStrategy" in {
      val prisoner = new Prisoner(0, 10, Strategies.titForTat)
      prisoner.nextAction() should be(Cooperate)

      prisoner.updateStrategy(Strategies.allDefect, None)
      prisoner.nextAction() should be(Defect)
      prisoner.payoff(3, Cooperate) should be(false)
      prisoner.nextAction() should be(Defect)
      prisoner.payoff(-1, Defect) should be(false)
      prisoner.nextAction() should be(Defect)

      prisoner.updateStrategy(Strategies.allCooperate, None)
      prisoner.nextAction() should be(Cooperate)
    }

    "return true for payoff if his energy equals 0" in {
      val prisoner = new Prisoner(0, 10, Strategies.titForTat)
      prisoner.payoff(-10, Cooperate) should be(true)
    }

    "return true for payoff if his energy is below 0" in {
      val prisoner = new Prisoner(0, 10, Strategies.titForTat)
      prisoner.payoff(-15, Cooperate) should be(true)
    }

    "reset strategy and energy" in {
      val prisoner = new Prisoner(0, 10, Strategies.titForTat)
      prisoner.payoff(-15, Cooperate) should be(true)
      prisoner.reset(Strategies.allDefect, 20)
      prisoner.payoff(-15, Cooperate) should be(false)
      prisoner.payoff(-15, Cooperate) should be(true)
      prisoner.nextAction() should be(PrisonerActor.Defect)
    }
  }
}

package pl.edu.agh.ipd.utils

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.Timeout

import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

abstract class IpdActorTest extends TestKit(ActorSystem("TestSystem")) with ImplicitSender with IpdTest {
  override implicit val patienceConfig: PatienceConfig = PatienceConfig(5 seconds, 50 milliseconds)
  implicit val akkaTimeout = Timeout(120 seconds)
  implicit val ec = system.dispatcher

  override def afterAll: Unit = {
    super.afterAll
    TestKit.shutdownActorSystem(system)
  }
}

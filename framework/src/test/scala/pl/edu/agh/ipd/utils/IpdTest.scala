package pl.edu.agh.ipd.utils

import org.scalatest.concurrent.Eventually
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

trait IpdTest extends WordSpecLike with Matchers with BeforeAndAfterAll with Eventually {
  override implicit val patienceConfig: PatienceConfig = PatienceConfig(1 second, 5 milliseconds)
}

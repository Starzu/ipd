package pl.edu.agh.ipd.gui

import io.udash._
import pl.edu.agh.ipd.gui.views._
import pl.edu.agh.ipd.gui.views.index.IndexViewPresenter

class StatesToViewPresenterDef extends ViewPresenterRegistry[RoutingState] {
  def matchStateToResolver(state: RoutingState): ViewPresenter[_ <: RoutingState] = state match {
    case RootState => RootViewPresenter
    case IndexState => IndexViewPresenter
    case _ => ErrorViewPresenter
  }
}
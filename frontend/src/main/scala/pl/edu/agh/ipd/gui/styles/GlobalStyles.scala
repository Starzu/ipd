package pl.edu.agh.ipd.gui.styles

import scala.concurrent.duration.DurationLong
import scala.language.postfixOps
import scalacss.Defaults._

object GlobalStyles extends StyleSheet.Inline {
  import dsl._

  val left = style(float.left)
  val right = style(float.right)

  val textCenter = style(textAlign.center)
}

package pl.edu.agh.ipd.gui.views

import io.udash._
import org.scalajs.dom.Element
import pl.edu.agh.ipd.gui.ErrorState

object ErrorViewPresenter extends DefaultViewPresenterFactory[ErrorState.type](() => new ErrorView)

class ErrorView extends FinalView {
  import scalatags.JsDom.all._

  override def getTemplate: Modifier =
    h3("URL not found!")
}
package pl.edu.agh.ipd.gui.views

import io.udash._
import io.udash.bootstrap.{BootstrapStyles, UdashBootstrap}
import io.udash.bootstrap.datepicker.UdashDatePicker
import org.scalajs.dom._
import org.scalajs.dom.raw.HTMLStyleElement
import pl.edu.agh.ipd.gui.styles.GlobalStyles

import scalatags.JsDom.tags2._

class RootView extends View {
  import scalacss.Defaults._
  import scalacss.ScalatagsCss._
  import scalatags.JsDom._
  import scalatags.JsDom.all._

  private val childView: Element = div().render

  override def getTemplate: Modifier =
    div(
      UdashBootstrap.loadBootstrapStyles(),
      UdashDatePicker.loadBootstrapDatePickerStyles(),
      UdashBootstrap.loadFontAwesome(),
      GlobalStyles.render[TypedTag[HTMLStyleElement]],
      main(BootstrapStyles.container)(childView)
    )

  override def renderChild(view: View): Unit = {
    while (childView.childElementCount > 0) childView.removeChild(childView.firstChild)
    view.getTemplate.applyTo(childView)
  }
}

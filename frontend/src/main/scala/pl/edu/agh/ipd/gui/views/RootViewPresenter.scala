package pl.edu.agh.ipd.gui.views

import io.udash.core.DefaultViewPresenterFactory
import pl.edu.agh.ipd.gui.RootState

object RootViewPresenter extends DefaultViewPresenterFactory[RootState.type](() => new RootView)

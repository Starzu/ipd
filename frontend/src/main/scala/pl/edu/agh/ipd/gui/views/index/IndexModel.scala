package pl.edu.agh.ipd.gui.views.index

import com.avsystem.commons.misc.{NamedEnum, NamedEnumCompanion}
import pl.edu.agh.ipd.gui.config.SimulationConfig
import pl.edu.agh.ipd.gui.config.SimulationConfig.ArenaMap.{GraphMap, TwoDimMap}
import pl.edu.agh.ipd.gui.config.SimulationConfig.Splitter.GraphSplitter

trait IndexModel {
  def config: SimulationConfig
  def mapConfig: MapConfig
  def splitterConfig: SplitterConfig

  def running: Boolean
  def stats: Seq[String]

  def warmupSeconds: Int
  def computationSeconds: Int
}

case class SplitterConfig(splitterVersion: SplitterConfig.SplitterVersion, graphSplitterOptions: GraphSplitter)

object SplitterConfig {
  sealed abstract class SplitterVersion(override val name: String) extends NamedEnum
  object SplitterVersion extends NamedEnumCompanion[SplitterVersion] {
    case object Dummy extends SplitterVersion("Dummy")
    case object Graph extends SplitterVersion("Graph")

    override val values: List[SplitterVersion] = caseObjects
  }
}

case class MapConfig(mapVersion: MapConfig.MapVersion, twoDimOptions: TwoDimMap, graphMapOptions: GraphMap)

object MapConfig {
  sealed abstract class MapVersion(override val name: String) extends NamedEnum
  object MapVersion extends NamedEnumCompanion[MapVersion] {
    case object OneDim extends MapVersion("One dim")
    case object TwoDim extends MapVersion("Two dims")
    case object Random extends MapVersion("Random")
    case object Graph extends MapVersion("Graph")

    override val values: List[MapVersion] = caseObjects
  }
}
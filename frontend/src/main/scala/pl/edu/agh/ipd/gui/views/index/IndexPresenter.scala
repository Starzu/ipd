package pl.edu.agh.ipd.gui.views.index

import io.udash._
import org.scalajs.dom
import pl.edu.agh.ipd.gui.IndexState

import scala.concurrent.Future
import scala.util.{Failure, Success}

class IndexPresenter(model: ModelProperty[IndexModel]) extends Presenter[IndexState.type] {
  import pl.edu.agh.ipd.gui.Context._

  dom.window.setInterval(() => {
    serverRpc.isRunning() onSuccess {
      case value => model.subProp(_.running).set(value)
    }
  }, 1000)

  override def handleState(state: IndexState.type): Unit = ()

  def startSimulation(): Unit = {
    model.subProp(_.running).set(true)
    serverRpc.start(model.subProp(_.config).get)
  }

  def stopSimulation(): Unit = {
    model.subProp(_.running).set(false)
    serverRpc.stop()
  }

  def loadStatistics(): Future[Unit] = {
    val req: Future[String] = serverRpc.stats()
    req onComplete {
      case Success(stats) =>
        model.subSeq(_.stats).append(stats)
      case Failure(e) => model.subSeq(_.stats).append(s"$e")
    }
    req.map(_ => ())
  }

  def resetStatistics(): Unit =
    serverRpc.resetStats()

  def clearStatistics(): Unit =
    model.subSeq(_.stats).clear()

  def automatedRun(): Unit = {
    val warmupTimeout: Double = model.subProp(_.warmupSeconds).get * 1000
    val compTime: Int = model.subProp(_.computationSeconds).get * 1000
    startSimulation()
    dom.window.setTimeout(() => resetStatistics(), warmupTimeout)
    dom.window.setTimeout(() => {
      loadStatistics() onComplete {
        case _ => stopSimulation()
      }
    }, warmupTimeout + compTime)
  }
}

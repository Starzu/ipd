package pl.edu.agh.ipd.gui.views.index

import io.udash._
import io.udash.bootstrap.button.{ButtonStyle, UdashButton, UdashButtonGroup}
import io.udash.bootstrap.form.UdashForm
import pl.edu.agh.ipd.gui.config.SimulationConfig.ArenaMap.{GraphMap, TwoDimMap}
import pl.edu.agh.ipd.gui.config.SimulationConfig.Splitter.GraphSplitter
import pl.edu.agh.ipd.gui.config.SimulationConfig._

class IndexView(model: ModelProperty[IndexModel], presenter: IndexPresenter) extends FinalView {
  import scalatags.JsDom.all._

  val startButton = UdashButton(ButtonStyle.Success)("Start")
  model.subProp(_.running).streamTo(startButton.disabled)(identity)
  startButton.listen {
    case _ => presenter.startSimulation()
  }

  val stopButton = UdashButton(ButtonStyle.Danger)("Stop")
  model.subProp(_.running).streamTo(stopButton.disabled)(!_)
  stopButton.listen {
    case _ => presenter.stopSimulation()
  }

  val resetStatsButton = UdashButton(ButtonStyle.Default)("Reset statistics")
  model.subProp(_.running).streamTo(resetStatsButton.disabled)(!_)
  resetStatsButton.listen {
    case _ => presenter.resetStatistics()
  }

  val getStatsButton = UdashButton(ButtonStyle.Default)("Get statistics")
  model.subProp(_.running).streamTo(getStatsButton.disabled)(!_)
  getStatsButton.listen {
    case _ => presenter.loadStatistics()
  }

  val clearStatsButton = UdashButton(ButtonStyle.Warning)("Clear statistics")
  clearStatsButton.listen {
    case _ => presenter.clearStatistics()
  }

  val automatedStartButton = UdashButton(ButtonStyle.Primary)("Run")
  model.subProp(_.running).streamTo(resetStatsButton.disabled)(!_)
  automatedStartButton.listen {
    case _ => presenter.automatedRun()
  }

  val stats = div(repeat(model.subSeq(_.stats))(s => pre(s.get).render))

  override def getTemplate: Modifier = {
    import pl.edu.agh.ipd.gui.Context._

    val availableHostsStr = Property[String](model.subProp(_.config.availableHosts).get.mkString(","))
    availableHostsStr.streamTo(model.subProp(_.config.availableHosts))(s => s.split(","))

    div(
      UdashForm(
        UdashForm.numberInput()("Population size (total)")(model.subProp(_.config.populationSize).transform(_.toString, _.toInt)),
        UdashForm.group(
          label("Initial strategy"),
          UdashForm.select(model.subProp(_.config.initialStrategy).transform(_.name, InitStrategy.byName), InitStrategy.values.map(_.name))
        ),
        UdashForm.numberInput()("Initial energy")(model.subProp(_.config.initialEnergy).transform(_.toString, _.toInt)),
        UdashForm.numberInput()("Temptation")(model.subProp(_.config.payoffs.temptation).transform(_.toString, _.toInt)),
        UdashForm.numberInput()("Reward")(model.subProp(_.config.payoffs.reward).transform(_.toString, _.toInt)),
        UdashForm.numberInput()("Punishment")(model.subProp(_.config.payoffs.punishment).transform(_.toString, _.toInt)),
        UdashForm.numberInput()("Sucker")(model.subProp(_.config.payoffs.sucker).transform(_.toString, _.toInt)),
        UdashForm.group(
          label("Map splitter"),
          UdashForm.select(model.subProp(_.splitterConfig.splitterVersion).transform(_.name, SplitterConfig.SplitterVersion.byName), SplitterConfig.SplitterVersion.values.map(_.name))
        ),
        produce(model.subProp(_.splitterConfig.splitterVersion)) {
          case SplitterConfig.SplitterVersion.Dummy => span().render
          case SplitterConfig.SplitterVersion.Graph => div(
            UdashForm.numberInput()("Filename")(model.subProp(_.splitterConfig.graphSplitterOptions).transform(_.filename, f => GraphSplitter(f)))
          ).render
        },
        UdashForm.checkbox()("Use stubs")(model.subProp(_.config.useStubs)),
        UdashForm.group(
          label("Prisoners map"),
          UdashForm.select(model.subProp(_.mapConfig.mapVersion).transform(_.name, MapConfig.MapVersion.byName), MapConfig.MapVersion.values.map(_.name))
        ),
        produce(model.subProp(_.mapConfig.mapVersion)) {
          case MapConfig.MapVersion.OneDim | MapConfig.MapVersion.Random => span().render
          case MapConfig.MapVersion.TwoDim => div(
            UdashForm.numberInput()("Width")(model.subProp(_.mapConfig.twoDimOptions).transform(_.width.toString, w => TwoDimMap(w.toInt)))
          ).render
          case MapConfig.MapVersion.Graph => div(
            UdashForm.numberInput()("Filename")(model.subProp(_.mapConfig.graphMapOptions).transform(_.filename, f => GraphMap(f)))
          ).render
        },
        UdashForm.numberInput()("Fight managers count (per node)")(model.subProp(_.config.fightMgrsCount).transform(_.toString, _.toInt)),
        UdashForm.numberInput()("Mutation managers count (per node)")(model.subProp(_.config.mutationMgrsCount).transform(_.toString, _.toInt)),
        UdashForm.numberInput()("Fight timeout [ms]")(model.subProp(_.config.fightTimeoutMs).transform(_.toString, _.toInt)),
        UdashForm.numberInput()("Stubs sync interval [ms]")(model.subProp(_.config.stubSyncIntervalMs).transform(_.toString, _.toInt)),
        UdashForm.numberInput()("Stubs sync batch threshold")(model.subProp(_.config.stubSyncBatchThreshold).transform(_.toString, _.toInt)),
        UdashForm.numberInput()("Local stats interval [ms]")(model.subProp(_.config.localStatsIntervalMs).transform(_.toString, _.toInt)),
        UdashForm.numberInput()("Master stats interval [ms]")(model.subProp(_.config.masterStatsIntervalMs).transform(_.toString, _.toInt)),
        UdashForm.textInput()("Available hosts (comma separated)")(availableHostsStr)
      ).render,
      span(
        UdashButtonGroup()(
          startButton.render,
          stopButton.render,
          getStatsButton.render,
          resetStatsButton.render
        ).render,
        clearStatsButton.render
      ),
      h3("Automated run"),
      UdashForm(
        UdashForm.numberInput()("Warmup [s]")(model.subProp(_.warmupSeconds).transform(_.toString, _.toInt)),
        UdashForm.numberInput()("Computation time [s]")(model.subProp(_.computationSeconds).transform(_.toString, _.toInt))
      ).render,
      automatedStartButton.render,
      stats
    )
  }
}

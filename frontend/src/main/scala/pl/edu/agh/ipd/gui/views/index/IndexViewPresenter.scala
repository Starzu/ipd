package pl.edu.agh.ipd.gui.views.index

import io.udash._
import pl.edu.agh.ipd.gui.IndexState
import pl.edu.agh.ipd.gui.config.SimulationConfig
import pl.edu.agh.ipd.gui.config.SimulationConfig.{ArenaMap, InitStrategy, Payoffs, Splitter}

object IndexViewPresenter extends ViewPresenter[IndexState.type] {
  import pl.edu.agh.ipd.gui.Context._

  override def create(): (View, Presenter[IndexState.type]) = {
    val model = ModelProperty[IndexModel]
    model.subProp(_.mapConfig).set(MapConfig(MapConfig.MapVersion.TwoDim, ArenaMap.TwoDimMap(width = 100), ArenaMap.GraphMap("10000.txt")))
    model.subProp(_.config).set(SimulationConfig(
      10000, InitStrategy.TitForTat, separateStrategies = true,  10, Payoffs(2, 1, 0, -1), Splitter.DummySplitter, useStubs = true,
      ArenaMap.TwoDimMap(100), 20.0, 100, 200, 20,
      replaceEnergyOnMutation = false, forceEpochDeaths = false, 1000, 0.0,
      60000, 10, 5000, 15000, 10, Seq("127.0.0.1:2552")))
    model.subProp(_.running).set(false)
    model.subSeq(_.stats).set(Seq.empty)

    model.subProp(_.splitterConfig).streamTo(model.subProp(_.config.splitter)) {
      case SplitterConfig(SplitterConfig.SplitterVersion.Dummy, _) => Splitter.DummySplitter
      case SplitterConfig(SplitterConfig.SplitterVersion.Graph, graphSplitterOptions) => graphSplitterOptions
    }

    model.subProp(_.mapConfig).streamTo(model.subProp(_.config.map)) {
      case MapConfig(MapConfig.MapVersion.OneDim, _, _) => ArenaMap.OneDimMap
      case MapConfig(MapConfig.MapVersion.TwoDim, twoDimOptions, _) => twoDimOptions
      case MapConfig(MapConfig.MapVersion.Random, _, _) => ArenaMap.Random
      case MapConfig(MapConfig.MapVersion.Graph, _, graphMapOptions) => graphMapOptions
    }

    model.subProp(_.warmupSeconds).set(60)
    model.subProp(_.computationSeconds).set(120)

    val presenter = new IndexPresenter(model)
    val view = new IndexView(model, presenter)
    (view, presenter)
  }
}

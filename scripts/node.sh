#!/bin/bash

cd $HOME/ipd

INSTANCES_PER_NODE=$1
IPD_CONFIG_TARGET_NAME=$2
WARMUP=$3
RUN=$4
CSV_INTERVAL=$5
CSV_NAME=$6

TASK_ID=$(($SLURM_LOCALID + $(($SLURM_NODEID * $SLURM_CPUS_ON_NODE))))

if [ "${SLURM_LOCALID}" -lt "${INSTANCES_PER_NODE}" ]; then
  NODE_CONFIG=${SLURM_JOBID}_${SLURM_NODEID}_${TASK_ID}.conf
  NODE_LOG=${SLURM_JOBID}_${SLURM_NODEID}_${TASK_ID}.log
  HOSTNAME=`hostname`
  PORT=`expr $SLURM_LOCALID + 2552`
  sed -e "s/127.0.0.1/${HOSTNAME}/g" node.tpl > ${SLURM_JOBID}_${SLURM_NODEID}_${TASK_ID}.template
  sed -e "s/2552/${PORT}/g" ${SLURM_JOBID}_${SLURM_NODEID}_${TASK_ID}.template > $NODE_CONFIG

  module load plgrid/tools/java8
  
  RAM=$((96 / $INSTANCES_PER_NODE))
  
  if [ "${TASK_ID}" -eq "0" ]; then
    sleep 10
    java -Xmx${RAM}G -cp ".:ipd.jar" pl.edu.agh.ipd.Launcher $NODE_CONFIG true $IPD_CONFIG_TARGET_NAME $WARMUP $RUN $CSV_INTERVAL $CSV_NAME > $NODE_LOG
  else
    java -Xmx${RAM}G -cp ".:ipd.jar" pl.edu.agh.ipd.Launcher $NODE_CONFIG true > $NODE_LOG
  fi;
fi;
from itertools import product
import os
from time import time

async = True
batch_size = 1
repeats = 1

nodes_nums = [10]
procs_per_node = 24 # const
instances_per_node = 1
assert (instances_per_node <= procs_per_node)
warmup = 300
run = 180
csv = 30

# all
populationSize = [1000000]
initialStrategy = ["Random"]  # "Random", "AllCooperate", "AllDefect"
separateStrategies = ["true"]
initialEnergy = [10]
payoffs = [[3,2,1,0]]
map = ["pl.edu.agh.ipd.actors.map.MapActorRouter"]
# map = ["pl.edu.agh.ipd.model.map.SimilarCooperativityNetwork"]
mapRoutees = [24]
mapRouteesClass = ["pl.edu.agh.ipd.actors.map.SimilarCooperativityMapActor"]
mapWidth = [100]
mapStructure = [1]
crossoverMap = ["pl.edu.agh.ipd.actors.map.MapActorRouter"]
# crossoverMap = ["pl.edu.agh.ipd.model.map.SimilarCooperativityNetwork"]
crossoverMapRoutees = [24]
crossoverMapRouteesClass = ["pl.edu.agh.ipd.actors.map.SimilarCooperativityMapActor"]
crossoverMapWidth = [100]
crossoverMapStructure = [1]
crossoverMapPresampleLocalStrategies = ["false"]
targetFightToMutationRatio = [100.0]
fightMgrsCount = [200]
mutationMgrsCount = [0]
replaceEnergyOnMutation = ["false"]
continuationProbability = [0]
topNStrategies = [10]
forceEpochDeaths = ["false"]

# async
mapSplitter = ["pl.edu.agh.ipd.actors.splitter.DummyArenaMapSplitter"] if async else ["unused"]
useStubs = ["false"] if async else ["false"]
crossoverMgrsCount = [100] if async else [1]
fightTimeoutMs = [5000] if async else [1]
stubSyncIntervalMs = [10000] if async else [1]
stubSyncBatchThreshold = [500] if async else [1]
localStatsIntervalMs = [60000] if async else [1]
masterStatsIntervalMs = [60000] if async else [1]

timestamp = int(time())
ind = 1
filenames = []
for (populationSize, initialStrategy, separateStrategies, initialEnergy, payoffs, map, mapRoutees, mapRouteesClass,
     mapWidth, mapStructure, crossoverMap, crossoverMapRoutees, crossoverMapRouteesClass, crossoverMapWidth,
     crossoverMapStructure, crossoverMapPresampleLocalStrategies, targetFightToMutationRatio, fightMgrsCount,
     mutationMgrsCount, replaceEnergyOnMutation, continuationProbability, topNStrategies, mapSplitter,
     useStubs, crossoverMgrsCount, fightTimeoutMs, stubSyncIntervalMs, stubSyncBatchThreshold,
     localStatsIntervalMs, masterStatsIntervalMs, forceEpochDeaths
     ) in product(populationSize, initialStrategy, separateStrategies, initialEnergy, payoffs, map, mapRoutees,
                  mapRouteesClass, mapWidth, mapStructure, crossoverMap, crossoverMapRoutees, crossoverMapRouteesClass,
                  crossoverMapWidth, crossoverMapStructure, crossoverMapPresampleLocalStrategies,
                  targetFightToMutationRatio, fightMgrsCount, mutationMgrsCount, replaceEnergyOnMutation,
                  continuationProbability, topNStrategies, mapSplitter, useStubs,
                  crossoverMgrsCount, fightTimeoutMs, stubSyncIntervalMs, stubSyncBatchThreshold, localStatsIntervalMs,
                  masterStatsIntervalMs, forceEpochDeaths):
    filename = "{}_{}.conf".format(timestamp, ind)
    mapFilename = "%d.txt" % populationSize
    crossoverMapFilename = "%d.txt" % populationSize
    splitterOptionsFilename = "%d.txt" % populationSize
    with open(filename, "w") as f:
        f.write("""
ipd {{
  populationSize = {0}
  initialStrategy = {1}
  separateStrategies = {2}
  initialEnergy = {3}
  temptation = {4}
  reward = {5}
  punishment = {6}
  sucker = {7}
  mapSplitter = {27}
  splitterOptions = {{
    width = {11}
    filename = {28}
  }}
  map = {8}
  mapOptions = {{
    routees = {9}
    routeesClass = {10}
    width = {11}
    filename = {12}
    structure = {13}
  }}
  crossoverMap = {14}
  crossoverMapOptions = {{
    routees = {15}
    routeesClass = {16}
    width = {17}
    filename = {18}
    structure = {19}
    presampleLocalStrategies = {20}
  }}
  useStubs = {29}
  targetFightsToMutationsRatio = {21}
  crossoverMgrsCount = {30}
  fightMgrsCount = {22}
  mutationMgrsCount = {23}
  replaceEnergyOnMutation = {24}
  forceEpochDeaths = {36}
  fightTimeoutMs = {31}
  continuationProbability = {25}
  stubSyncIntervalMs = {32}
  stubSyncBatchThreshold = {33}
  localStatsIntervalMs = {34}
  masterStatsIntervalMs = {35}
  topNStrategies = {26}
  availableHosts = []
}}
    """.format(populationSize, initialStrategy, separateStrategies, initialEnergy, payoffs[0], payoffs[1], payoffs[2],
               payoffs[3], map, mapRoutees, mapRouteesClass,
               mapWidth, mapFilename, mapStructure, crossoverMap, crossoverMapRoutees, crossoverMapRouteesClass,
               crossoverMapWidth,
               crossoverMapFilename, crossoverMapStructure, crossoverMapPresampleLocalStrategies,
               targetFightToMutationRatio, fightMgrsCount,
               mutationMgrsCount, replaceEnergyOnMutation, continuationProbability, topNStrategies, mapSplitter,
               splitterOptionsFilename, useStubs, crossoverMgrsCount, fightTimeoutMs, stubSyncIntervalMs,
               stubSyncBatchThreshold,
               localStatsIntervalMs, masterStatsIntervalMs, forceEpochDeaths))
        ind += 1
        filenames.append(filename)

for start in range(0, len(filenames), batch_size):
    files = filenames[start:(start+batch_size if len(filenames) > start+batch_size else len(filenames))]
    if async:
        for nodes_num in nodes_nums:
            procs_num = nodes_num * procs_per_node
            os.system("sbatch -p plgrid -N {} -n {} -c 1 --mem-per-cpu=4G --time=00:10:00 -A pracam $HOME/ipd/hosts.sh {} {} {} {} {}"
            .format(nodes_num, procs_num, instances_per_node, warmup, run, csv, " ".join(files * repeats)))
    else:
        os.system("sbatch -p plgrid -N 1 -n 1 -c 1 --mem-per-cpu=4G --time=00:05:00 -A pracam $HOME/ipd/seq.sh {} {} {}"
              .format(run, csv, " ".join(files * repeats)))

package pl.edu.agh.ipd.gui.config

import com.avsystem.commons.misc.{NamedEnum, NamedEnumCompanion}

case class SimulationConfig(populationSize: Int,
                            initialStrategy: SimulationConfig.InitStrategy,
                            separateStrategies: Boolean,
                            initialEnergy: Double,
                            payoffs: SimulationConfig.Payoffs,
                            splitter: SimulationConfig.Splitter,
                            useStubs: Boolean,
                            map: SimulationConfig.ArenaMap,
                            targetFightsToMutationsRatio: Double,
                            crossoverMgrsCount: Int,
                            fightMgrsCount: Int,
                            mutationMgrsCount: Int,
                            replaceEnergyOnMutation: Boolean,
                            forceEpochDeaths: Boolean,
                            fightTimeoutMs: Int,
                            continuationProbability: Double,
                            stubSyncIntervalMs: Int,
                            stubSyncBatchThreshold: Int,
                            localStatsIntervalMs: Int,
                            masterStatsIntervalMs: Int,
                            topNStrategies: Int,
                            availableHosts: Seq[String])

object SimulationConfig {
  sealed abstract class InitStrategy(val name: String) extends NamedEnum
  object InitStrategy extends NamedEnumCompanion[InitStrategy] {
    case object TitForTat extends InitStrategy("TitForTat")
    case object AllCooperate extends InitStrategy("AllCooperate")
    case object AllDefect extends InitStrategy("AllDefect")
    case object Random extends InitStrategy("Random")

    override val values: List[InitStrategy] = caseObjects
  }

  sealed abstract class ArenaMap(val name: String, val className: String)
  object ArenaMap {
    case object OneDimMap extends ArenaMap("OneDimMap", "pl.edu.agh.ipd.actors.map.OneDimArenaMapActor")
    case class TwoDimMap(width: Int) extends ArenaMap("TwoDimMap", "pl.edu.agh.ipd.actors.map.TwoDimArenaMapActor")
    case class GraphMap(filename: String) extends ArenaMap("GraphMap", "pl.edu.agh.ipd.actors.map.GraphMapActor")
    case object Random extends ArenaMap("Random", "pl.edu.agh.ipd.actors.map.RandomArenaMapActor")
  }

  sealed abstract class Splitter(val name: String, val className: String)
  object Splitter {
    case object DummySplitter extends Splitter("DummySplitter", "pl.edu.agh.ipd.actors.splitter.DummyArenaMapSplitter")
    case class GraphSplitter(filename: String) extends Splitter("GraphSplitter", "pl.edu.agh.ipd.actors.splitter.GraphMapSplitter")
  }

  case class Payoffs(temptation: Double, reward: Double, punishment: Double, sucker: Double) {
    // TODO Udash bug - fixed in 0.5.0
//    require(temptation > reward)
//    require(reward > punishment)
//    require(punishment > sucker)
  }
}
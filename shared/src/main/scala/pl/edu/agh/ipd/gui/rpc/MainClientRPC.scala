package pl.edu.agh.ipd.gui.rpc

import com.avsystem.commons.rpc.RPC

@RPC
trait MainClientRPC {
  def push(number: Int): Unit
}
       
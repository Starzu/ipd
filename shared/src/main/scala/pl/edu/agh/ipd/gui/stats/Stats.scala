package pl.edu.agh.ipd.gui.stats

case class Stats(energySum: Double,
                 energyAvg: Double,
                 energyMax: Double,
                 energyMin: Double,

                 fightsSum: Long,
                 fightsAvg: Long,
                 fightsMax: Long,
                 fightsMin: Long,

                 allTimeouts: Long,
                 remoteTimeouts: Long,
                 remoteFightsStarted: Long,

                 lastCooperationsCount: Long,
                 deaths: Long,

                 mutationsSum: Long,
                 mutationsAvg: Long,
                 mutationsMax: Long,
                 mutationsMin: Long)
